# Newcity Apartments presentation website

## General:
  * Developped by: [Schon Web Development](#) & [Genuineq](https://www.genuineq.com)

## Technical details:
  * PHP Version 7.2
  * Wordpress Version 5.0.3
  * Base theme: [Understrap 0.8.0](https://understrap.com)

## Link:
  * http://newcity-apartments.ro
