��          \       �       �      �   X   �      �      	            Q   (    z     �  `   �     �     �     �     �  7      Accept By using this form you agree with the storage and handling of your data by this website. My settings Note Save my settings Settings These settings will only apply to the browser and device you are currently using. Project-Id-Version: WP GDPR Compliance
POT-Creation-Date: 2018-11-12 14:17+0100
Last-Translator: michael <rotarumichael@gmail.com>
Language-Team: Romanian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-03-11 11:21+0000
X-Generator: Loco https://localise.biz/
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_ngettext:1,2;_n;_ngettext_noop:1,2;_n_noop:1,2;_c;_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Poedit-SearchPath-0: .
Report-Msgid-Bugs-To: 
Language: ro_RO
Plural-Forms: nplurals=3; plural=(n==1 ? 0 :(((n%100>19)||(( n%100==0)&&(n!=0)))? 2 : 1));
X-Loco-Version: 2.2.0; wp-5.0.3 Accepta Utilizand acest formular sunteti decord cu stocarea si manipularea datelor cu caracter personal. Setari Nota Salveaza Setari Aceste setari vor fi aplicate doar browser-ului curent. 