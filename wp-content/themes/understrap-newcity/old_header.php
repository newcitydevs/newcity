<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>
    <div class="site" id="page"><!-- page-wrapper-start -->

      <!-- ******************* The Navbar Area ******************* -->
      <div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite"><!-- wrapper-navbar-start -->
        <div class="info-nav-wrapper"><!-- info-navigation-start -->
          <div class="container">
            <div class="row justify-content-md-center">
              <div class="col-6 col-md-auto info-nav-item">
                <img src="http://newcity.test/wp-content/uploads/2019/01/logo-newcity.png" class="info-nav-item-logo">
              </div>

              <div class="col-6 col-md-auto info-nav-item">
                <div class="info-nav-item-content-wrapper">
                  <i class="fa fa-phone fa-lg info-nav-item-content-icon"></i>

                  <span class="info-nav-item-content-text ml-1">
                    <span class="info-nav-item-content-text-title">Telefon</span>
                    <span class="info-nav-item-content-text-content">0743 222 333</span>
                  </span>
                </div>
              </div>
              <div class="col-12 col-md-auto info-nav-item">
                <div class="info-nav-item-content-wrapper">
                  <i class="fa fa-at fa-lg info-nav-item-content-icon"></i>

                  <span class="info-nav-item-content-text ml-1">
                    <span class="info-nav-item-content-text-title">Email</span>
                    <span class="info-nav-item-content-text-content">contact@newcityapartaments.ro</span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div><!-- info-navigation-end -->

        <nav class="nav justify-content-center menu-nav py-2"><!-- menu-navigation-start -->
          <!-- The WordPress Menu goes here -->
          <?php
            wp_nav_menu([ 'menu'            => 'menu-primary'
                        , 'menu_class'      => 'nav justify-content-center'
                        , 'menu_id'         => 'menu_primary'
                        , 'container'       => ''
                        , 'container_class' => ''
                        , 'container_id'    => ''
                        , 'fallback_cb'     => false
                        , 'depth'           => 0
                        , 'walker'          => new Understrap_WP_Bootstrap_Navwalker()
                        , 'theme_location'  => 'primary']);
          ?>
        </nav><!-- menu-navigation-end -->
      </div><!-- wrapper-navbar-end -->
<div class="site" id="page">
	<div class="info-section-header">
		<div class="container">
			<div class="row">
				<div class="col-2 offset-3">
					<img src=<?php echo get_site_url()."/wp-content/uploads/2019/01/logo-newcity.png";?>>
				</div>
					<div class="col-2 left-icon-wp">
						<div class="row left-icon-row">
							<div class="col-3 icon-col">
								<div class="icon-box">
									<i class="fa fa-phone fa-lg"></i>
								</div>
							</div>
							<div class="col-9 text-col">
								<span class="info-strong">Telefon</span><br/>
								<span class="info-light">0743 222 333</span>
							</div>
					</div>
				</div>
				<div class="col-2 left-icon-wp">
					<div class="row left-icon-row">
						<div class="col-3 icon-col">
							<div class="icon-box">
								<i class="fa fa-envelope fa-lg"></i>
							</div>
						</div>
						<div class="col-9 text-col">
							<span class="info-strong">Email</span><br/>
							<span class="info-light">contact@newcityapartaments.ro</span>
						</div>
				</div>
			</div>
			</div>
		</div>
	</div>

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite"><!-- wrapper-navbar-start -->
    <!-- <nav class="nav justify-content-center menu-nav py-2">
			<nav class="nav justify-content-center py-2 info-nav">
				<ul class="nav info-nav-wrapper">
					<li class="nav-item info-nav-item">
						<img src=<?php echo get_site_url()."/wp-content/uploads/2019/01/logo-newcity.png";?> class="info-nav-item-logo">
					</li>

					<li class="nav-item info-nav-item ml-3">
						<div class="info-nav-item-content-wrapper">
							<i class="fa fa-phone fa-lg info-nav-item-content-icon"></i>

							<span class="info-nav-item-content-text ml-1">
								<span class="info-nav-item-content-text-title">Telefon</span><br/>
								<span class="info-nav-item-content-text-content">0743 222 333</span>
							</span>
						</div>
					</li>
					<li class="nav-item info-nav-item ml-3">
						<div class="info-nav-item-content-wrapper">
							<span class="info-nav-item-content-text ml-1">
								<span class="info-nav-item-content-text-title">Email</span><br/>
								<span class="info-nav-item-content-text-content">contact@newcityapartaments.ro</span>
							</span>
						</div>
					</li>
				</ul>
			</nav> -->
      <!-- The WordPress Menu goes here -->
      <?php
        wp_nav_menu([ 'menu'            => 'menu-primary'
                    , 'menu_class'      => 'nav justify-content-center'
                    , 'fallback_cb'     => false
                    , 'menu_id'         => 'main_menu'
                    , 'depth'           => 0
                    , 'walker'          => new Understrap_WP_Bootstrap_Navwalker()
                    , 'theme_location'  => 'primary'
                    , 'container'       => '']);
      ?>
		</nav><!-- menu-navigation-end -->
	</div><!-- wrapper-navbar-end -->
