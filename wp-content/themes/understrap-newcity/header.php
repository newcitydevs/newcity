<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>
    <div class="site" id="page"><!-- page-wrapper-start -->

      <!-- ******************* The Navbar Area ******************* -->
      <div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite"><!-- wrapper-navbar-start -->
        <div class="info-nav-wrapper p-2"><!-- info-navigation-start -->
          <div class="container">
            <div class="row justify-content-md-center">

              <div class="col-12 col-md-auto info-nav-item">
                <a href="tel:+40755075701">
                  <div class="info-nav-item-content-wrapper">
                    <i class="fa fa-phone fa-lg info-nav-item-content-icon"></i>
                    <span class="info-nav-item-content-text ml-1">
                      <span class="info-nav-item-content-text-title">Telefon</span>
                      <span class="info-nav-item-content-text-content">0755 075 701</span>
                    </span>
                  </div>
                </a>
              </div>
              <div class="col-12 col-md-auto info-nav-item">
                <a href="mailto:office@newcity-apartments.ro">
                  <div class="info-nav-item-content-wrapper">
                    <i class="fa fa-at fa-lg info-nav-item-content-icon"></i>
                    <span class="info-nav-item-content-text ml-1">
                      <span class="info-nav-item-content-text-title">Email</span>
                      <span class="info-nav-item-content-text-content">office@newcity-apartments.ro</span>
                    </span>
                  </div>
                </a>
              </div>

              <div class="col-12 col-md-auto info-nav-item">
                <a href="https://www.facebook.com/newcityapartments/" target="_blank">
                  <div class="info-nav-item-content-wrapper">
                    <i class="fa fa-facebook-f fa-lg info-nav-item-content-icon"></i>
                  </div>
                <a>
              </div>
            </div>

          </div>
        </div><!-- info-navigation-end -->

        <div class="d-none d-md-block"><?php hm_get_template_part( 'partials/offer_bar'); ?></div>

        <div class="menu-nav py-2">
          <div class="container-fluid">
            <div class="row">
              <div class="offset-0 offset-xl-1 col-3 col-xl-2">
                  <?php
                      $page_id = get_queried_object_id();
                      $logo_path = site_url() . "/wp-content/images/logo-apartments-min.png";
                      $home_url = site_url();
                      // if($page_id == 429 || (isset($_GET['complex']) && $_GET['complex'] == '44')){
                      if((isset($_GET['complex']) && $_GET['complex'] == '44')){
                        $logo_path = site_url() . "/wp-content/images/logo-cetatii-min.png";
                        $home_url = site_url() ."/descriere-cetatii";
                      // }else if($page_id == 443 || (isset($_GET['complex']) && $_GET['complex'] == '38')){
                      }else if((isset($_GET['complex']) && $_GET['complex'] == '38')){
                        $logo_path = site_url() . "/wp-content/images/logo-eroilor-min.png";
                        $home_url = site_url() ."/descriere-eroilor";
                      }
                  ?>
                  <a href="<?= $home_url; ?>"><img src="<?= $logo_path; ?>" class="info-nav-item-logo"></a>
              </div>
              <div class="col-9 col-xl-8">
          <!-- The WordPress Menu goes here -->
          <?php
            wp_nav_menu([ 'menu'            => 'menu-primary'
                        , 'menu_class'      => 'nav'
                        , 'menu_id'         => 'menu_primary'
                        , 'container'       => ''
                        , 'container_class' => ''
                        , 'container_id'    => ''
                        , 'fallback_cb'     => false
                        , 'depth'           => 0
                        , 'walker'          => new Understrap_WP_Bootstrap_Navwalker()
                        , 'theme_location'  => 'primary']);
          ?>
          </div>
        </div>
      </div>
    </div>

  </div><!-- wrapper-navbar-end -->
  <div class="d-block d-md-none"><?php hm_get_template_part( 'partials/offer_bar'); ?></div>
