<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>

<div class="container vertical-description-wrapper partial bg-white">
  <div class="row">
    <div class="col-12">
      <h2 class="vertical-description-text-title"><?php echo $template_args['description-text-title']; ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-md-6 vertical-description-picture-wrapper">
      <img src="<?php echo $template_args['description-picture']; ?>" class="vertical-description-picture align-self-center">
    </div>



    <div class="col-12 col-md-6">
      <?php foreach ($template_args['description-text-content-left'] as $line){ ?>
        <p class="vertical-description-text-content"><?php echo $line; ?></p>
      <?php } ?>
      <div class="vertical-description-text-wrapper">
        <ul class="vertical-description-text-list bullet-list">
          <?php foreach ($template_args['description-text-content-right'] as $line){ ?>
            <li>
              <span class="vertical-description-text-content"><?php echo $line; ?></span>
            </li>
          <?php } ?>
        </ul>
      </div>
    </div>
    </div>
</div>
