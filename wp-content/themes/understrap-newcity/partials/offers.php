<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>

<div class="container offers__wrapper partial bg-white">
  <div class="container">
    <div class="row">
        <div class="col-12 offers__title">
          <h2 class="m-0"><?php echo $template_args['title']; ?></h2>
        </div>
    </div>
    <div class="row offers__items-container">
    <?php foreach ($template_args['items'] as $item){ ?>
      <div class="col-12 col-sm-6 col-md-6 col-lg-4 p-0">
        <div class="offers__item-wrapper">
          <div class="row">
            <div class="col-12">
              <img src="<?php echo $item['picture']; ?>" class="offers__picture">
              <div class="offers__highline">
                <p><?php echo $item['highline']; ?></p>
              </div>
            </div>
          </div>

          <div class="container p-3">
            <div class="row">
              <div class="col-12">
                <h3 class="m-0 offers__item-title"><?php echo $item['title']; ?></h3>
              </div>
              <div class="col-12 ">
                <p class="offers__text--weight">indepand de la &euro;<?php echo $item['price']; ?>&#8725;m<sup>2</sup></p>
              </div>
            </div>

            <div class="row">
              <div class="col-12 col-md-6">
                <p class="offers__surface-tag"><?php echo $item['surface']['tag']; ?>: </p>
                <p class="offers__surface-value"><?php echo $item['surface']['value']; ?> m<sup>2</sup></p>
              </div>
              <div class="col-12 col-md-6 offers__item_buttons-wp">
                <button class="button dark-button-contour" value="<?= $item['buttons']['details']; ?>"><span><?= $item['buttons']['details']; ?><span></button>
                <button class="button dark-button" value="<?= $item['buttons']['offer']; ?>"><span><?= $item['buttons']['offer']; ?><span></button>
              </div>
              <!-- <div class="col-12 col-md-6 offers__details">
                <?php foreach ($item['details'] as $detail){ ?>
                  <p><strong><?php echo $detail['tag']; ?>:</strong> <?php echo $detail['value']; ?> m<sup>2</sup></p>
                <?php } ?>
              </div> -->
            </div>

          </div>
        </div>
      </div>
    <?php } ?>
    </div>
  </div>
  </div>
</div>
