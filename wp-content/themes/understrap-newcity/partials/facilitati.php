<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>
<?php hm_get_template_part( 'partials/horizontal_media_list'
                          , [ 'media-list-title' => "CONFORTUL PE CARE TI-L OFERIM"
                            , 'media-list-content' => [ [ 'media-list-content-picture' => site_url() . "/wp-content/images/facilitati/parcare.jpg"
                                                        , 'media-list-content-title' => "Parcare"
                                                        , 'media-list-content-text' => "Deoarece stim cat de important este locul de parcare, New City Apartments integreaza in ansambluri rezidentiale pe care le realizeaza atat locuri de parcare exterioare cat si subterane."]
                                                      , [ 'media-list-content-picture' => site_url() . "/wp-content/images/facilitati/parcuri.jpg"
                                                        , 'media-list-content-title' => "Parcuri"
                                                        , 'media-list-content-text' => "Aerisit, spatios dar si luminos, ansamblul nostru este inconjurat de spatii verzi, cu parcuri generoase si locuri de joaca pentru copii la fiecare bloc. De asemenea, vom punem la dispozitie alei pe care te poti plimba sau unde poti chiar alerga."]
                                                      // , [ 'media-list-content-picture' => site_url() . "/wp-content/images/facilitati/securitate.jpg"
                                                      //   , 'media-list-content-title' => "Siguranta locatarilor"
                                                      //   , 'media-list-content-text' => "Aceasta este asigurata prin accesul controlat in incinta ansamblului, precum si prin supravegherea video 24/24 a complexului, inclusiv a parcarilor, pentru a garanta siguranta bunurilor si a tuturor spatiilor comune."]
                                                      , [ 'media-list-content-picture' => site_url() . "/wp-content/images/facilitati/confort.jpg"
                                                        , 'media-list-content-title' => "Tehnologie"
                                                        , 'media-list-content-text' => "Apartamentele realizate de noi dispun de centrala termica Ariston / IMMERGAS, calorifere, usa metalica la intrarea in apartament, cu un design modern, instalatii electrice, termice si sanitare trase la pozitie."]
                                                      , [ 'media-list-content-picture' => site_url() . "/wp-content/images/facilitati/personalizare.jpg"
                                                        , 'media-list-content-title' => "Personalizeaza-ti locuinta"
                                                        , 'media-list-content-text' => "Apartamentele se predau in stadiul <strong style='font-weight:600'>semifinisat</strong>, acest lucru insemnand tencuieli si strat de glet care va permite ulterior alegerea unei culori preferate, sapa, tamplarie Rehau sau <strong style='font-weight:600'>finisat</strong> oferind clientului posibilitatea de a-si alege finisajele preferate din cataloagele noastre."]]]); ?>
