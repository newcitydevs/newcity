<?php
/**
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="advanced-counter counter <?= $template_args['css-class']; ?>">
	<span class="counter__head"><span class="available"><?= $template_args['free-key']; ?></span>/<?= $template_args['total-key']; ?></span>
	<p class="counter__num">
		<!-- @set AVAILABLE / @set TOTAL -->
		<span class="available"><?= $template_args['free-value']; ?></span>/<span class="total"><?= $template_args['total-value']; ?></span>
	</p>
	<div class="counter__progress">
		<span class="counter__progress-total"></span>
		<!-- @set WIDTH PERCENT MIN 0% - MAX 100% -->
		<span class="counter__progress-available" style="width:<?= (($template_args['free-value']/$template_args['total-value']) * 100); ?>%;"></span>
	</div>
	<!-- @set DESCRIPTION -->
	<span class="counter__desc"><?= $template_args['description-intro']; ?><span class="available">&nbsp;<?= $template_args['description-finish']; ?></span></span>
</div>
