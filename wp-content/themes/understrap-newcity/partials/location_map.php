<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>

<div class="location-map__wrapper--background partial">
  <div class="container location-map__container--color">
    <div class="row">
      <div class="col-12">
        <h2><?php echo $template_args['title']; ?></h2>
        <p class="location-map__address--color">
          <i class="fa fa-map-marker"></i> <?php echo $template_args['address']; ?>
        </p>
        <hr class="my-1 ml-0 location-map__separator--color" />
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <div class="location-map__separator--responsive-map">
          <iframe src="<?php echo $template_args['map']; ?>" width="600" height="450" frameborder="0" style="border:0"></iframe>
        </div>
        <hr class="my-1 ml-0 location-map__separator--color" />
      </div>
    </div>

    <?php if (isset($template_args['items'])) { ?>
      <div class="row my-2">
        <?php foreach ($template_args['items'] as $item){ ?>
          <div class="col-12 col-md-4">
            <p class="location-map__info--title"><?php echo $item['title']['up']; ?></p>
            <hr class="my-1 ml-0 location-map__info-separator--color" />
            <p class="location-map__info--title"><?php echo $item['title']['down']; ?></p>
            <p class="location-map__info--description"><?php echo $item['description']; ?></p>
          </div>
        <?php } ?>
      </div>
    <?php } ?>
  </div>
</div>
