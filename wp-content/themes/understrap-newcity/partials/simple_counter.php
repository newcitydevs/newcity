<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>

<div class="counter">
	<!-- @set COUNT -->
	<span class="counter__num"><?= $template_args['value']; ?></span>
	<hr class="m-1 half-thick-separator"/>
	<!-- @set DESCRIPTION -->
	<span class="counter__desc"><?= $template_args['description']; ?></span>
</div>
