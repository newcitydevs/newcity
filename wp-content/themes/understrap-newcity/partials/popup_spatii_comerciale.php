<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>

<div class="modal fade" id="popup_pdf" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-header__content">
          <h2 class="popup_form--title">Spatii Comerciale</h2>
          <a class="modal-button__download js-download-plan" target="_blank" href="#">descarca fisierul in format PDF</a>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body popup_pdf--wrapper">
        <iframe id="js-pdf-iframe" src="" width="600" height="450" frameborder="0" style="border:0"></iframe>
      </div>
    </div>
  </div>
</div>

<script>
  $('#popup_pdf').on('show.bs.modal', function (event) {
    /* Button that triggered the modal. */
    var button = $(event.relatedTarget);
    /* Extract info from data-* attributes. */
    var plan = button.data('plan');

    /* Update the modal's content. */
    var modal = $(this)
    modal.find('#js-pdf-iframe').attr('src', plan);
    modal.find(".js-download-plan").attr('href', plan);
  })

  //Hide/Show pdf popup trigger if is touche device or not
  $(function(){
    var wrapper = $(".pdf-buttons-wrapper");
    if(Modernizr.touchevents){
      wrapper.find(".js-desktop-btn").removeClass("d-inline-block").addClass("d-none");
      wrapper.find(".js-mobile-btn").removeClass("d-none").addClass("d-inline-block");
    }else{
      wrapper.find(".js-desktop-btn").removeClass("d-none").addClass("d-inline-block");
      wrapper.find(".js-mobile-btn").removeClass("d-inline-block").addClass("d-none");
    }
  })
</script>
