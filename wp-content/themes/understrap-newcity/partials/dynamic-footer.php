<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */
if ( ! defined( 'ABSPATH' ) ) {exit; /* Exit if accessed directly. */} ?>
      <div class="footer-wrapper partial"><!-- footer-wrapper-start -->
        <div class="container">
          <div class="row justify-content-md-center">
            <div class="col-12 col-md-6 p-4 footer-item">
              <a href="<?=site_url()?>"><img src="<?=site_url()?>/wp-content/uploads/2019/03/logo-apartments.png" class="static-footer-logo"></a>
            </div>

            <div class="col-12 col-md-6 footer-item contact-info">
              <?php hm_get_template_part( 'partials/contact_info',[]); ?>
            </div>
          </div>
        </div>

      <?php get_footer(); ?>
  </body>
</html>
