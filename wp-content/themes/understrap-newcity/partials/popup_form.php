<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>

<div class="modal fade" id="popup_form" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="popup_form--title">Cere o oferta de pret!</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <?= do_shortcode('[contact-form-7 id="458" title="Contact form 1"]'); ?>
      </div>
    </div>
  </div>
</div>
