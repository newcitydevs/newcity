<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>

<div class="container-fluid testimonials__wrapper partial">
  <div class="row p-0">
    <div class="col-12 p-0">
      <?php echo do_shortcode('[testimonial_rotator id=469]'); ?>
    </div>
  </div>
</div>
