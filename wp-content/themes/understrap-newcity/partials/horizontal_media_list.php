<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>

<div class="container horizontal-media-list-wrapper partial bg-white">
  <div class="row">
    <div class="col-12 col-md-12 pb-1">
      <h2 class="m-0"><?php echo $template_args['media-list-title']; ?></h2>
    </div>
  </div>
  <div class="row">
    <?php foreach ($template_args['media-list-content'] as $line){ ?>
      <div class="col-6 col-sm-6 col-md horizontal-media-list-item">
        <img src="<?php echo $line['media-list-content-picture']; ?>" class="horizontal-media-list-content-picture">
        <p class="horizontal-media-list-content-title m-0"><?php echo $line['media-list-content-title']; ?></p>
        <p class="horizontal-media-list-content-text m-0"><?php echo $line['media-list-content-text']; ?></p>
      </div>
    <?php } ?>
    </div>
  </div>
</div>
