<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/}
$offer_content = get_option('offer_bar_text', '');
?>
<?php if(strlen($offer_content) != 0):?>
<div class="container-fluid offer-bar">
    <div class="row justify-content-md-center">
      <p><?php echo $offer_content; ?></p>
    </div>
</div>
<?php endif;?>
