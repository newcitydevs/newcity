<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/}
    // $count = 0;
     foreach ($template_args['items'] as $item){
    //     $count = $count + 1;
    //     $lg_col = 6;
    //     if(sizeof($template_args['items'])%2 == 1 && sizeof($template_args['items']) - $count == 0){
    //       $lg_col = 12;
    //     }
?>

        <div class="col-sm-12 col-lg col-xl-4 complex-menu__item hover-gradient <?= (isset($item['custom_class'])) ? ($item['custom_class']) : (''); ?>">
          <div class="vertical-black-gradient background-section__wrapper">
            <div class="background-section__image" style="background-image:url(<?= $item['background']; ?>)"></div>
          </div>
          <a href=<?= $item['url']?> class="complex-menu__item-content">
            <div class="complex-title__wp <?= (isset($item['title']['style'])) ? ($item['title']['style']) : (''); ?>">
              <h2 class="complex-title__firstline"><?= $item['title']['firstline-f']; ?><strong><?= $item['title']['firstline-s']; ?></strong></h2>
              <?php if (isset($item['title']['secondline'])) { ?>
                  <h2 class="complex-title__secondline"><?= $item['title']['secondline']; ?></h2>
              <?php } ?>
              <button class="button red-button" value="<?= $item['button']['text']; ?>" style="<?= (isset($item['button']['style'])) ? ($item['button']['style']) : (''); ?>"><span><?= $item['button']['text']; ?></span></button>
            </div>
            <div class="container complex-menu__report m-0 p-0">
              <div class="row m-0 p-4">
                <?php foreach ($item['counters'] as $counter){ ?>
                  <div class="col-4 col-md p-1"><?= hm_get_template_part( 'partials/simple_counter', ['value' => (isset($counter['value'])) ? ($counter['value']) : (""), 'description' => (isset($counter['description'])) ? ($counter['description']) : ("")]); ?></div>
                <?php } ?>
              </div>
            </div>
          </a>
        </div>


<?php } ?>
