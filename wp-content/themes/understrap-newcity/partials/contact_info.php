<?php $template_args = [ 'logo' => site_url() . "/wp-content/uploads/2019/03/logo-apartments.png"
                       , 'brand' => 'NEWCITY-APARTMENTS.RO'
                       , 'schedule' => ['label' => 'Luni - Vineri:', 'value' => '09:00-12:00 | 13:00-17:00, Sambata: 11:00-17:00']
                       , 'address' => ['label' => 'Adresa:', 'value' => 'Str. Eroilor, nr. 378a, Ap. 59, Parter, Comuna Floresti, Jud. Cluj']
                       , 'sales' => ['label' => 'Telefon vanzari:', 'value' => '0758 828 282 / 0746 090 909']
                       , 'secretary' => ['label' => 'Alexandra Tulbure:', 'value' => '0755 075 701']
                       , 'email' => ['label' => 'Email:', 'value' => 'office@newcity-apartments.ro']
                       , 'copyright' => 'NewCityApartments - Toate drepturile rezervate.']; ?>


<div class="align-middle p-1">
  <!-- <h4 class="footer-item-title"><?= $template_args['brand']; ?></h4> -->
  <!-- <hr class="my-1 ml-0 footer-text-separator thick-separator" /> -->
  <span class="footer-item-text"><strong><?= $template_args['schedule']['label']; ?>&nbsp;</strong><?= $template_args['schedule']['value']; ?></span>
  <hr class="my-1 ml-0 footer-text-separator thick-separator" />
  <span class="footer-item-text"><strong><?= $template_args['address']['label']; ?>&nbsp;</strong><?= $template_args['address']['value']; ?></span>
  <span class="footer-item-text"><strong><?= $template_args['sales']['label']; ?>&nbsp;</strong><?= $template_args['sales']['value']; ?></span>
  <span class="footer-item-text"><strong><?= $template_args['secretary']['label']; ?>&nbsp;</strong><?= $template_args['secretary']['value']; ?></span>
  <span class="footer-item-text"><strong><?= $template_args['email']['label']; ?>&nbsp;</strong><?= $template_args['email']['value']; ?></span>
</div>
