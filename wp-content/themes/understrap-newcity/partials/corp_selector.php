<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>
  <div class="container corp_selector__wrapper ">
    <div class="row px-1">
      <div class="col-12 col-md-12 corp_selector__title">
        <p class="m-0"><?= $template_args['title']; ?></p>
      </div>
    </div>
    <div class="row corp_selector__items-wp">
      <?php for ($index = 1; $index <= $template_args['count']; $index++){?>
        <div class="col corp_selector__item <?= ( (isset($template_args['disable']) && (TRUE == $template_args['disable'][$index - 1])) ? ('disabled') : ('') ); ?> <?= ( (isset($template_args['selected']) && ($index == $template_args['selected'])) ? ('corp_selector__item--selected') : ('') ); ?>">
          <div class="row">
            <a href="<?= $template_args['url'] . $index; ?>">
            <div class="col-12 p-2 corp_selector__item-content">
              <p class="m-0">Bloc</p>
            </div>
            <div class="col-12 p-2 corp_selector__item-content">
              <p class="corp_selector__item-number"><?= $index; ?></p>
            </div>
            <div class="col-12 p-2 corp_selector__item-content">
              <p class="m-0" style="font-weight:bold"><?= isset($template_args['details'][$index - 1]) ? $template_args['details'][$index - 1] : '&nbsp;' ?></p>
            </div>
            </a>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
&nbsp;
