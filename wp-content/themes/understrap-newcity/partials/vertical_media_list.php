<?php if ( ! defined( 'ABSPATH' ) ) {exit; /*Exit if accessed directly.*/} ?>

<div class="container vertical-media-list-wrapper partial bg-white">

  <?php foreach ($template_args['media-list-content'] as $line){ ?>
    <div class="row vertical-media-list-content">
      <?php switch ($line['media-list-content-orientation']) {
        case "left": ?>
          <div class="col-12 col-md-6">
            <img class="align-self-center vertical-media-list-content-picture mr-3 p-0" src="<?php echo $line['media-list-content-picture']; ?>">
          </div>
          <div class="col-12 col-md-6 vertical-media-list-content-wrapper">
            <h2 class="vertical-media-list-content-title m-0"><?php echo $line['media-list-content-title']; ?></h2>
            <p class="vertical-media-list-content-text"><?php echo $line['media-list-content-text']; ?></p>
            <?php if(isset($line['media-list-content-button'])){ ?>
              <button class="button dark-button vertical-media-list-content-button" value="<?= $line['media-list-content-button']; ?>"><span><?= $line['media-list-content-button']; ?><span></button>
            <?php } ?>
          </div>
          <?php break; ?>
        <?php case "right": ?>
          <div class="col-12 col-md-6 vertical-media-list-content-wrapper">
            <h2 class="vertical-media-list-content-title m-0"><?php echo $line['media-list-content-title']; ?></h2>
            <p class="vertical-media-list-content-text"><?php echo $line['media-list-content-text']; ?></p>
            <?php if(isset($line['media-list-content-button'])){ ?>
              <button class="button dark-button vertical-media-list-content-button" value="<?= $line['media-list-content-button']; ?>"><span><?= $line['media-list-content-button']; ?><span></button>
            <?php } ?>
          </div>
          <div class="col-12 col-md-6 order-first order-md-last">
            <img class="align-self-center vertical-media-list-content-picture mr-3 p-0" src="<?php echo $line['media-list-content-picture']; ?>">
          </div>
          <?php break; ?>
      <?php } ?>
    </div>
  <?php } ?>

</div>
