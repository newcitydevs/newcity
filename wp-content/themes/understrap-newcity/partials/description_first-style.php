<?php if (! defined('ABSPATH')) {
    exit; /*Exit if accessed directly.*/
} ?>

<div class="container horizontal-description-wrapper partial bg-white">
  <div class="row">
    <div class="col-12">
        <h2 class="horizontal-description-text-title"><?php echo $template_args['description-text-title']; ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-md-6">
      <div class="horizontal-description-text-wrapper align-middle">
        <?php foreach ($template_args['description-text-content'] as $line) {?>
          <p class="horizontal-description-text-content"><?php echo $line; ?></p>
        <?php } ?>
      </div>
    </div>
    <div class="col-12 col-md-6">
      <img src="<?php echo $template_args['description-picture']; ?>" class="horizontal-description-picture align-self-center">
    </div>
  </div>
</div>
