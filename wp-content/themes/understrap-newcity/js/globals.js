$ = jQuery;
$(document).ready(function () {
    // Handler for .ready() called.

    if(findGetParameter('filtru')){
      scrollTo($('.js-apartments-filter').offset().top,0);
    }else{
      if(findGetParameter('bloc')){
        scrollTo($('.js-floor-menu').offset().top);
      }
    }
    $(".js-floor-menu-btn").on("click",function(){
      scrollTo($('.js-floor-menu').offset().top);
    })

    //Change lightbox options
    try{
      lightbox.option({
          'resizeDuration' : 0,
          'wrapAround' : true,
          'fadeDuration' : 0,
          'imageFadeDuration' : 0
      });
    }catch(e){}

    ///Force add contact info to mobile menu
    var contact_info = $(".info-nav-wrapper").clone();
    $(".shiftnav-inner").prepend(contact_info);
    $(".shiftnav-inner").find(".col-md-auto").each(function(){$(this).removeClass("col-md-auto")});

    ///Force add toggle function to mobile menu
    $(".sub-menu").slideUp();
    $(".shiftnav-nav .menu-item-has-children>a").on("click",function(e){e.preventDefault();$(this).parent().find(".sub-menu").toggle();})
});

$(window).load(function () {
    //Add Lightbox close button to top
    $(".lb-outerContainer").each(function() {
        var item = $(this);
        item.insertAfter(item.next());
    });
});

var scrollTo = function(val,speed = 'medium'){
  $('html, body').animate({
      scrollTop: val
  }, speed);
}

var findGetParameter = function(parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

//autocomplete apartment location on Get Offer form
$(function(){
  $(".get-offer-ap_location").css("display","none");
  $(".js-get_offer-btn").on("click",function(e){$("input[name='apartament']").val($(this).closest(".apartment__row").find("input[name='full_ap_location']").val())})
})
