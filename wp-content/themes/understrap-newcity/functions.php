<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_enqueue_styles() {
	// Get the theme data
	$the_theme = wp_get_theme();
	$version = false;//$the_theme->get( 'Version' );
		wp_enqueue_style( 'raleway_font', 'https://fonts.googleapis.com/css?family=Raleway:300,400,600,700,800&display=swap', false );
		wp_enqueue_style( 'open_sans_font', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&display=swap', false );
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $version );
    wp_enqueue_script( 'jquery');
		wp_enqueue_script('globals', get_stylesheet_directory_uri() . '/js/globals.js', array('jquery'), $version, false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $version, false );
		wp_enqueue_script('modernizr', get_stylesheet_directory_uri() . '/js/modernizr-custom.js', $version, true);
	  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );

/**
 * Like get_template_part() but lets you pass args to the template file
 * Args are available in the template as $template_args array
 * @param string filepart
 * @param mixed wp_args style argument list
 */
function hm_get_template_part( $file, $template_args = array(), $cache_args = array() ) {
  $template_args = wp_parse_args( $template_args );
  $cache_args = wp_parse_args( $cache_args );
  if ( $cache_args ) {
      foreach ( $template_args as $key => $value ) {
          if ( is_scalar( $value ) || is_array( $value ) ) {
              $cache_args[$key] = $value;
          } else if ( is_object( $value ) && method_exists( $value, 'get_id' ) ) {
              $cache_args[$key] = call_user_method( 'get_id', $value );
          }
      }
      if ( ( $cache = wp_cache_get( $file, serialize( $cache_args ) ) ) !== false ) {
          if ( ! empty( $template_args['return'] ) )
              return $cache;
          echo $cache;
          return;
      }
  }
  $file_handle = $file;
  do_action( 'start_operation', 'hm_template_part::' . $file_handle );
  if ( file_exists( get_stylesheet_directory() . '/' . $file . '.php' ) )
      $file = get_stylesheet_directory() . '/' . $file . '.php';
  elseif ( file_exists( get_template_directory() . '/' . $file . '.php' ) )
      $file = get_template_directory() . '/' . $file . '.php';
  ob_start();
  $return = require( $file );
  $data = ob_get_clean();
  do_action( 'end_operation', 'hm_template_part::' . $file_handle );
  if ( $cache_args ) {
      wp_cache_set( $file, $data, serialize( $cache_args ), 3600 );
  }
  if ( ! empty( $template_args['return'] ) )
      if ( $return === false )
          return false;
      else
          return $data;
  echo $data;
}


/**
 * Function that is executed after each post type is registered
 *  and it maked the posts hierarchical
 *
 * @param post_type The post type
 * @param pto
 */
function newcity_make_posts_hierarchical($post_type, $pto){
  /* Return, if not post type posts. */
  if ($post_type != 'post'){ return; }

  /* Access $wp_post_types global variable. */
  global $wp_post_types;

  /* Set post type "post" to be hierarchical. */
  $wp_post_types['post']->hierarchical = 1;

  /* Add page attributes to post backend. */
  /* This adds the box to set up parent and menu order on edit posts. */
  add_post_type_support( 'post', 'page-attributes' );
}
add_action('registered_post_type', 'newcity_make_posts_hierarchical', 10, 2 );


/**
 * Function that extracts the total number of apartments with 1, 2 and 3 rooms,
 *  the total apartments surface and the number of parking spots of a complex.
 *
 * @param int complex_id: The id of the complex post.
 * @param int one_room_count: Output parameter that holds the number of one room apartments.
 * @param int two_room_count: Output parameter that holds the number of two room apartments.
 * @param int three_room_count: Output parameter that holds the number of three room apartments.
 * @param int total_surface: Output parameter that holds the total surface of all the apartments.
 * @param int parking_spots_count: Output parameter that holds the total number of parking spots.
 */
function get_complex_characteristics($complex_id, &$one_room_count, &$two_room_count, &$three_room_count, &$total_surface, &$parking_spots_count){
  /* Extract the complex post. */
  $complex_query = new WP_Query([ 'p' => /*ID*/$complex_id, 'post_type' => 'post' ]);
  if($complex_query->have_posts()){
    /* Save the complex post. */
    $complex = $complex_query->posts[0];

    /* Extract total number of parking spots of the complex. */
    $parking_spots_count = get_post_meta($complex->ID, 'locuri-parcare', TRUE);
    $parking_spots_count = ("" == $parking_spots_count) ? (0) : ($parking_spots_count);

    /* Reset wp_query loop. */
    wp_reset_postdata();

    /* Extract the buildings posts. */
    $buildings_query = new WP_Query([ 'category__in' => /*building*/3, 'post_parent' => $complex->ID, 'orderby' => 'name', 'posts_per_page' => -1, 'order' => 'ASC' ]);
    if($buildings_query->have_posts()){
      foreach ($buildings_query->posts as $key => $building) {
        /* Extract the floors posts. */
        $floors_query = new WP_Query([ 'category__in' => /*floor*/4, 'post_parent' => $building->ID, 'orderby' => 'meta_value_num', 'meta_key' => 'numar-etaj', 'posts_per_page' => -1, 'order' => 'DESC' ]);

        if($floors_query->have_posts()){
          foreach ($floors_query->posts as $key => $floor) {
            $one_room_apartments_query = new WP_Query([ 'category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_key' => 'numar-camere', 'meta_value' => '1', 'meta_compare' => '=', 'posts_per_page' => -1 ]);
            $one_room_count += $one_room_apartments_query->post_count;

            foreach ($one_room_apartments_query->posts as $key => $apartment) {
              $surface = get_post_meta($apartment->ID, 'suprafata-utila', TRUE);
              $total_surface += ("" != $surface) ? (floatval($surface)) : (0);
            }
            wp_reset_postdata();

            $two_room_apartments_query = new WP_Query([ 'category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_key' => 'numar-camere', 'meta_value' => '2', 'meta_compare' => '=', 'posts_per_page' => -1 ]);
            $two_room_count += $two_room_apartments_query->post_count;

            foreach ($two_room_apartments_query->posts as $key => $apartment) {
              $surface = get_post_meta($apartment->ID, 'suprafata-utila', TRUE);
              $total_surface += ("" != $surface) ? (floatval($surface)) : (0);
            }
            wp_reset_postdata();

            $three_room_apartments_query = new WP_Query([ 'category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_key' => 'numar-camere', 'meta_value' => '3', 'meta_compare' => '=', 'posts_per_page' => -1 ]);
            $three_room_count += $three_room_apartments_query->post_count;

            foreach ($three_room_apartments_query->posts as $key => $apartment) {
              $surface = get_post_meta($apartment->ID, 'suprafata-utila', TRUE);
              $total_surface += ("" != $surface) ? (floatval($surface)) : (0);
            }
            wp_reset_postdata();
          }
          /* Reset wp_query loop. */
          wp_reset_postdata();
        }
      }
      /* Reset wp_query loop. */
      wp_reset_postdata();
    }

    /* Round the total surface. */
    $total_surface = round($total_surface);
  }
}


////////////////////////
//////////ADMIN/////////
////////////////////////

//HIDE ALL ADMIN NOTICES
function hide_update_noticee_to_all_but_admin_users()
{
    if (!WP_DEBUG_DISPLAY) {
        remove_all_actions('admin_notices');
    }
}
add_action('admin_head', 'hide_update_noticee_to_all_but_admin_users', 1);

function add_simple_counter() {
    get_template_part( 'components/simple', 'counter' );
}
add_action( 'simple-counter', 'add_simple_counter' );
