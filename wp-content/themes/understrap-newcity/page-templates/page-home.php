<?php
/**
 * Template Name: Home Page Template
 *
 * Template for displaying the home page.
 *
 * @package understrap-newcity
 */

if ( ! defined( 'ABSPATH' ) ) {exit; /* Exit if accessed directly.*/} ?>

<?php get_header(); ?>

<?php
  /****************** EROILOR data extract START ********************/
  $one_room_total_count_apartments_eroilor = 0;
  $two_room_total_count_apartments_eroilor = 0;
  $three_room_total_count_apartments_eroilor = 0;
  $total_surface_eroilor = 0;
  $total_parking_spots_eroilor = 0;

  get_complex_characteristics( /*EROILOR_ID*/38
                             , $one_room_total_count_apartments_eroilor
                             , $two_room_total_count_apartments_eroilor
                             , $three_room_total_count_apartments_eroilor
                             , $total_surface_eroilor
                             , $total_parking_spots_eroilor);
  /****************** EROILOR data extract FINISH ********************/

  /****************** CETATII data extract START ********************/
  $one_room_total_count_apartments_cetatii = 0;
  $two_room_total_count_apartments_cetatii = 0;
  $three_room_total_count_apartments_cetatii = 0;
  $total_surface_cetatii = 0;
  $total_parking_spots_cetatii = 0;

  get_complex_characteristics( /*CETATII_ID*/44
                             , $one_room_total_count_apartments_cetatii
                             , $two_room_total_count_apartments_cetatii
                             , $three_room_total_count_apartments_cetatii
                             , $total_surface_cetatii
                             , $total_parking_spots_cetatii);
  /****************** CETATII data extract FINISH ********************/
?>

<div class="container-fluid complex-menu__container partial bg-white">
  <div class="row">
    <?php hm_get_template_part( 'partials/complex_selector'
                              , [ 'items' => [ ['url' => site_url() . "/apartamente/?complex=38"
                                               ,'background' => site_url() . "/wp-content/images/eroilor/eroilor-thumb.jpg"
                                               , 'title' => ['firstline-f'=> "New", 'firstline-s' => "City", 'secondline' => "Eroilor"]
                                               , 'button' => ['style' => "", 'text' => "Vezi detalii"]
                                               , 'counters' => [ ['value' => $one_room_total_count_apartments_eroilor, 'description' => "apartamente cu 1 camera"]
                                                               , ['value' => $two_room_total_count_apartments_eroilor, 'description' => "apartamente cu 2 camera"]
                                                               , ['value' => $three_room_total_count_apartments_eroilor, 'description' => "apartamente cu 3 camera"]
                                                               , ['value' => $total_surface_eroilor, 'description' => "m<sup>2</sup> de spatii de locuit"]
                                                               , ['value' => $total_parking_spots_eroilor, 'description' => "locuri de parcare"]]]

                                             , ['url' => site_url() . "/apartamente/?complex=44"
                                               ,'background' => site_url() . "/wp-content/images/cetatii/cetatii-thumb.jpg"
                                               , 'title' => ['firstline-f'=> "New", 'firstline-s' => "City", 'secondline' => "cetatii"]
                                               , 'button' => ['style' => "", 'text' => "Vezi detalii"]
                                               , 'counters' => [ ['value' => $one_room_total_count_apartments_cetatii, 'description' => "apartamente cu 1 camera"]
                                                               , ['value' => $two_room_total_count_apartments_cetatii, 'description' => "apartamente cu 2 camera"]
                                                               , ['value' => $three_room_total_count_apartments_cetatii, 'description' => "apartamente cu 3 camera"]
                                                               , ['value' => $total_surface_cetatii, 'description' => "m<sup>2</sup> de spatii de locuit"]
                                                               , ['value' => $total_parking_spots_cetatii, 'description' => "locuri de parcare"]]]

    ]]);?>


    <!-- CUSTOM ELEMENT START -->
    <?php
      $custom_el_background = site_url() . "/wp-content/images/spatii-comerciale/spatii-thumb.jpg";
      $custom_el_title = "Spatii <strong>Comerciale</strong>";
      $custom_el_class = "";
      $custom_el_first_url = site_url() . "/wp-content/uploads/planuri-spatii/spatii-comerciale-eroilor.pdf";
      $custom_el_second_url = site_url() . "/wp-content/uploads/planuri-spatii/spatii-comerciale-cetatii.pdf";
    ?>

    <div class="col-sm-12 col-lg col-xl-4 complex-menu__item <?= $custom_el_class; ?>">
      <div class="vertical-black-gradient background-section__wrapper">
        <div class="background-section__image" style="background-image:url(<?= $custom_el_background; ?>)"></div>
      </div>
      <div class="complex-menu__item-content">
        <div class="complex-title__wp">
          <h2 class="complex-title__firstline"><?= $custom_el_title ?></h2>
          <div class="col-12 col-md-12 mt-2 complex-menu__location-menu pdf-buttons-wrapper">
            <button type="button" class="button red-button js-desktop-btn d-none mr-2" data-toggle="modal" data-target="#popup_pdf" data-plan="<?= $custom_el_first_url ?>" value="Spatii Comerciale Eroilor"><span>Eroilor<span></button>
            <a class="button red-button js-mobile-btn d-inline-block mr-2" target="_blank" href="<?= $custom_el_first_url ?>"><span>Eroilor</span></a>

            <button type="button" class="button red-button js-desktop-btn d-none ml-2" data-toggle="modal" data-target="#popup_pdf" data-plan="<?= $custom_el_second_url ?>" value="Spatii Comerciale Cetatii"><span>Cetatii<span></button>
            <a class="button red-button js-mobile-btn d-inline-block mr-2" target="_blank" href="<?= $custom_el_first_url ?>"><span>Cetatii</span></a>
          </div>
        </div>
      </div>
    </div>
    <!-- CUSTOM ELEMENT END -->



  </div>

</div>

	<div class="container horizontal-media-list-wrapper partial bg-white">
	  <div class="row">

	    <div class="col-12">
	      <h2 class="">Urmareste-ne pe <a href="https://www.youtube.com/channel/UCUmu1xY9maTNIKiDFBR49yg" target="_blank" class="highlight" style="color:red">Youtube</a></h2>
	    </div>
	    <div class="col-12">
	      <iframe width="100%" height="677" src="https://www.youtube.com/embed/tZPLWcnAQGQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	    </div>

	    </div>
	  </div>
	</div>

<?php //hm_get_template_part( 'partials/testimonials'); ?>
<?php hm_get_template_part( 'partials/popup_spatii_comerciale'); ?>
<?php hm_get_template_part( 'partials/dynamic-footer'); ?>
