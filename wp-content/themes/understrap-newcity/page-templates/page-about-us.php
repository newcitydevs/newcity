<?php
/**
 * Template Name: Abount Us Page Template
 *
 * Template for displaying the "Abount Us" info.
 *
 * @package understrap-newcity
 */
if ( ! defined( 'ABSPATH' ) ) {exit; /* Exit if accessed directly.*/} ?>

<?php get_header(); ?>

<div class="container page-contact__wrapper p-0">
  <div class="row page-title">
    <div class="col-12">
      <h1 class="page-title__content">DESPRE NOI</h1>
    </div>
  </div>


<?php hm_get_template_part( 'partials/description_first-style'
                          , [ 'description-text-title' => "CINE SUNTEM?"
                          , 'description-text-content' => [ "Suntem o companie dinamica, perfecta pentru persoanele si familiile aflate la inceput de drum, dar si pentru cei care doresc sa se bucure de liniste, aer proaspat si confort, toate acestea regasite din plin in ansamblul nostru."
                                                            , "Aerisit, spatios si luminos, complexul nostru este inconjurat de spatii verzi, parcuri si locuri de joaca pentru copii la fiecare bloc. In ansamblul New City Apartments veti gasi echilibrul perfect intre o viata sanatoasa si una dinamica, urbana"
                                                            , "New City Apartments va propune o comunitate vibranta perefcta pentru orice familie."]
                          , 'description-picture' => site_url() . "/wp-content/images/aboutus.jpg"]);
?>

<?php hm_get_template_part( 'partials/description_second-style'
                          , [ 'description-text-title' => "MATERIALE SI TEHNOLOGII"
                            , 'description-picture' => site_url() . "/wp-content/images/materials.jpg"
                            , 'description-text-content-left' => ["Calitatea constructiei reprezinta pentru noi unul din cele mai importante aspecte, in care investim cu incredere. Astfel, materialele si tehnologiile utilizate se incadreaza in cele mai exigente standarde, facand parte doar din gamele superioare si premium:"]
                            , 'description-text-content-right' =>
                            [ "Caramida POROTHERM",
                              "Tamplarie cu 3 foi de sticla REHAU",
                              "Izolatie de exterior - polistiren expandat ignifugat ESP-80 de 10cm BAUMIT",
                              "Izolatie termica si fonica intre placi din beton celular",
                              "Centrala termica ARISTON / IMMERGAS",
                              "Calorifere",
                              "Usa metalica la intrarea in apartament (90cm)",
                              "Instalatii electrice, termice si sanitare",
                              "Parchet egger (8mm)",
                              "Gresie si faianta DELTA STUDIO (30x60cm)",
                              "Zugraveli lavabile",
                              "Usi de interior PORTA DOORS",
                              "Prize si intrerupatoare"
                            ]
                            ]); ?>

<?php hm_get_template_part( 'partials/facilitati') ?>
</div>

<?php hm_get_template_part( 'partials/dynamic-footer'); ?>
