<?php
/**
 * Template Name: Contact Page Template
 *
 * Template for displaying the contact page.
 *
 * @package understrap-newcity
 */

if ( ! defined( 'ABSPATH' ) ) {exit; /* Exit if accessed directly.*/} ?>

<?php get_header(); ?>


<div class="container page-contact__wrapper ">
  <div class="row page-title">
    <div class="col-12">
      <h1 class="page-title__content">CONTACTEAZA-NE!</h1>
    </div>
  </div>

  <div class="container partial bg-white">
  <div class="row py-1">
    <div class="col-12">
      <h2>TRIMITE-NE MESAJUL TAU</h2>
    </div>
  </div>

  <div class="row py-1">
    <div class="col-12 col-md-6">
      <?= do_shortcode('[contact-form-7 id="8" title="Contact form 1"]'); ?>
    </div>

    <div class="col-12 col-md-6 page-contact__company-data">
      <?php hm_get_template_part( 'partials/contact_info'); ?>
    </div>
  </div>
</div>
</div>

<?php hm_get_template_part( 'partials/location_map'
                          , [ 'title' => "LOCALIZARE"
                            , 'address' => "Str. Eroilor, nr. 378, Comuna Floresti, Jud. Cluj"
                            , 'map' => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2734.6054572061735!2d23.482809489857082!3d46.733242922537485!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4749102f152e44f5%3A0xf3aa7b1c8eda5026!2sNew+City+Eroilor!5e0!3m2!1sen!2sro!4v1549275780973"
                            ]); ?>

<?php hm_get_template_part( 'partials/dynamic-footer'); ?>
