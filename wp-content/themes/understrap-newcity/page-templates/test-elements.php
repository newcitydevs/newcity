<?php
/**
 * Template Name: Sample Elements
 *
 * Template for displaying the contact page.
 *
 * @package understrap-newcity
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
// $container = get_theme_mod( 'understrap_container_type' );
$container = "container-fluid";
?>



<?php hm_get_template_part( 'partials/offers'
                           , [ 'title' => "Oferta Apartamente Eroilor"
                           , 'items' => [ [ 'picture' => site_url() . "/wp-content/images/aboutus.jpg"
                                          , 'highline' => "REDUCERE 20%"
                                          , 'title' => "APARTAMENT 3 CAMERE"
                                          , 'price' => "400"
                                          , 'surface' => [ 'tag' => "Suprafata utila", 'value' => "36.98" ]
                                          , 'details' => [ ['tag' => "Baie", 'value' => '5.42'], ['tag' => "Bucatarie", 'value' => "11.23"], ['tag' => "Camera de zi", 'value' => "18.15"], ['tag' => "Hol", 'value' => "1.95"] ]
                                          , 'buttons' => [ 'details' => "VEZI DETALII", 'offer' => "CERE OFERTA"]]
                                        , [ 'picture' => site_url() . "/wp-content/images/aboutus.jpg"
                                          , 'highline' => "REDUCERE 20%"
                                          , 'title' => "APARTAMENT 3 CAMERE"
                                          , 'price' => "400"
                                          , 'surface' => [ 'tag' => "Suprafata utila", 'value' => "36.98" ]
                                          , 'details' => [ ['tag' => "Baie", 'value' => '5.42'], ['tag' => "Bucatarie", 'value' => "11.23"], ['tag' => "Camera de zi", 'value' => "18.15"], ['tag' => "Hol", 'value' => "1.95"] ]
                                          , 'buttons' => [ 'details' => "VEZI DETALII", 'offer' => "CERE OFERTA"]]
                                        , [ 'picture' => site_url() . "/wp-content/images/aboutus.jpg"
                                          , 'highline' => "REDUCERE 20%"
                                          , 'title' => "APARTAMENT 3 CAMERE"
                                          , 'price' => "400"
                                          , 'surface' => [ 'tag' => "Suprafata utila", 'value' => "36.98" ]
                                          , 'details' => [ ['tag' => "Baie", 'value' => '5.42'], ['tag' => "Bucatarie", 'value' => "11.23"], ['tag' => "Camera de zi", 'value' => "18.15"], ['tag' => "Hol", 'value' => "1.95"] ]
                                          , 'buttons' => [ 'details' => "VEZI DETALII", 'offer' => "CERE OFERTA"]]
                                        , [ 'picture' => site_url() . "/wp-content/images/aboutus.jpg"
                                          , 'highline' => "REDUCERE 20%"
                                          , 'title' => "APARTAMENT 3 CAMERE"
                                          , 'price' => "400"
                                          , 'surface' => [ 'tag' => "Suprafata utila", 'value' => "36.98" ]
                                          , 'details' => [ ['tag' => "Baie", 'value' => '5.42'], ['tag' => "Bucatarie", 'value' => "11.23"], ['tag' => "Camera de zi", 'value' => "18.15"], ['tag' => "Hol", 'value' => "1.95"] ]
                                          , 'buttons' => [ 'details' => "VEZI DETALII", 'offer' => "CERE OFERTA"]]
                                        , [ 'picture' => site_url() . "/wp-content/images/aboutus.jpg"
                                          , 'highline' => "REDUCERE 20%"
                                          , 'title' => "APARTAMENT 3 CAMERE"
                                          , 'price' => "400"
                                          , 'surface' => [ 'tag' => "Suprafata utila", 'value' => "36.98" ]
                                          , 'details' => [ ['tag' => "Baie", 'value' => '5.42'], ['tag' => "Bucatarie", 'value' => "11.23"], ['tag' => "Camera de zi", 'value' => "18.15"], ['tag' => "Hol", 'value' => "1.95"] ]
                                          , 'buttons' => [ 'details' => "VEZI DETALII", 'offer' => "CERE OFERTA"]]
                                        , [ 'picture' => site_url() . "/wp-content/images/aboutus.jpg"
                                          , 'highline' => "REDUCERE 20%"
                                          , 'title' => "APARTAMENT 3 CAMERE"
                                          , 'price' => "400"
                                          , 'surface' => [ 'tag' => "Suprafata utila", 'value' => "36.98" ]
                                          , 'details' => [ ['tag' => "Baie", 'value' => '5.42'], ['tag' => "Bucatarie", 'value' => "11.23"], ['tag' => "Camera de zi", 'value' => "18.15"], ['tag' => "Hol", 'value' => "1.95"] ]
                                          , 'buttons' => [ 'details' => "VEZI DETALII", 'offer' => "CERE OFERTA"]]
                                        ]]);
?>

<div class="container partial bg-white">
	<div class="row">
		<div class="col-12">
				<h2 class="horizontal-description-text-title">Parteneri</h2>
		</div>
	</div>
	<div class="mx-auto">
		<?php echo do_shortcode("[smls id='459']"); ?>
	</div>
</div>


<?php hm_get_template_part( 'partials/horizontal_media_list'
                          , [ 'media-list-title' => "FACILITATI APARTAMENTE"
                            , 'media-list-content' => [ [ 'media-list-content-picture' => site_url() . "/wp-content/images/aboutus.jpg"
                                                        , 'media-list-content-title' => "Tehnologii"
                                                        , 'media-list-content-text' => "Spatii exterioare amenajate se specialistii Paghera, Italia"]
                                                      , [ 'media-list-content-picture' => site_url() . "/wp-content/images/aboutus.jpg"
                                                        , 'media-list-content-title' => "Design"
                                                        , 'media-list-content-text' => "Spatii exterioare amenajate se specialistii Paghera, Italia"]
                                                      , [ 'media-list-content-picture' => site_url() . "/wp-content/images/aboutus.jpg"
                                                        , 'media-list-content-title' => "Securitate"
                                                        , 'media-list-content-text' => "Spatii exterioare amenajate se specialistii Paghera, Italia"]
                                                      , [ 'media-list-content-picture' => site_url() . "/wp-content/images/aboutus.jpg"
                                                        , 'media-list-content-title' => "Parcuri"
                                                        , 'media-list-content-text' => "Spatii exterioare amenajate se specialistii Paghera, Italia"]
                                                      , [ 'media-list-content-picture' => site_url() . "/wp-content/images/aboutus.jpg"
                                                        , 'media-list-content-title' => "Parcare"
                                                        , 'media-list-content-text' => "Spatii exterioare amenajate se specialistii Paghera, Italia"]]]);
?>



<!-- FLOOR SELECTOR START -->
<div class="container-fluid floor-menu__container">
	<div class="row">
		<div class="col-10 col-md-8 col-lg-8 col-xl-6 floor-menu__info-side">
			<div class="floor-menu__complex-info">
				<h1 class="floor-menu__complex-info--name">New City <strong>Eroilor</strong></h1>
				<p class="floor-menu__complex-info--description">
					Pentru a fi siguri ca in complexul New City Apartment este un loc si pentru dumneavoastra, venim cu o gama variata de apartamente de 1, 2 si 3 camere. Pentru a le studia indeaproape este suficient sa va alegeti etajul dorit, iar apoi sa
				</p>
				<h2 class="floor-menu__complex-info--location">
					<span>Corp <span class="js-corp-num">6</span>,</span><span> Etaj <span class="js-etaj-num">4</span></span>
				</h2>
				<div class="floor-menu__complex-info--avability col-12">
					<div><?php do_action( 'advanced-counter' ); ?></div>
					<div><?php do_action( 'advanced-counter' ); ?></div>
					<div><?php do_action( 'advanced-counter' ); ?></div>
				</div>
			</div>
		</div>
		<div class="col-2 col-md-4 col-lg-4 col-xl-6 floor-menu__building-side">
			<div class="background-section__wrapper">
				<div class="background-section__image" style="background-image:url('http://localhost:8888/newcity/wp-content/images/aboutus.jpg')">
					<nav class="floor-menu__nav">
						<a>4</a>
						<a>3</a>
						<a>2</a>
						<a>1</a>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- FLOOR SELECTOR END -->






	<div class="<?php echo esc_attr( $container ); ?>" id="content" >
			<div class="wrapper" id="full-width-page-wrapper" >
			  <div class="row">
				  <div class="col-md-12 content-area" id="primary">
					  <main class="site-main" id="main" role="main">







							<!-- COMPLEX SELECTOR START -->
							<div class="container-fluid complex-menu__container" style="">
							  <div class="row">
							    <div class="col-sm-12 col-lg-6 col-xl-4 complex-menu__item">
										<div class="vertical-black-gradient background-section__wrapper">
										  <div class="background-section__image" style="background-image:url('http://localhost:8888/newcity/wp-content/images/aboutus.jpg')"></div>
										</div>
										<div class="complex-menu__item-content">
											<div class="complex-menu__title-wp">
												<h2 class="complex-menu__title-firstline">New<strong>City</strong></h2>
												<h2 class="complex-menu__title-secondline">Eroilor</h2>
												<button class="button light-button" value="Vezi detalii"><span>Vezi detalii</spam></button>
											</div>
											<div class="container complex-menu__report">
												<div class="row">
													<div class="col-2 offset-1 p-1"><?php do_action( 'simple-counter' ); ?></div>
													<div class="col-2 p-1"><?php do_action( 'simple-counter' ); ?></div>
													<div class="col-2 p-1"><?php do_action( 'simple-counter' ); ?></div>
													<div class="col-2 p-1"><?php do_action( 'simple-counter' ); ?></div>
													<div class="col-2 p-1"><?php do_action( 'simple-counter' ); ?></div>
												</div>
											</div>
										</div>
							    </div>
									<div class="col-sm-12 col-lg-6 col-xl-4 complex-menu__item">
										<div class="vertical-black-gradient background-section__wrapper">
										  <div class="background-section__image" style="background-image:url('http://localhost:8888/newcity/wp-content/images/aboutus.jpg')"></div>
										</div>
										<div class="complex-menu__item-content">
											<div class="complex-menu__title-wp">
												<h2 class="complex-menu__title-firstline">New<strong>City</strong></h2>
												<h2 class="complex-menu__title-secondline">cetatii</h2>
												<button class="button light-button" value="Vezi detalii"><span>Vezi detalii</spam></button>
											</div>
											<div class="complex-menu__report container">
												<div class="row">
													<div class="col-2 offset-1 p-1"><?php do_action( 'simple-counter' ); ?></div>
													<div class="col-2 p-1"><?php do_action( 'simple-counter' ); ?></div>
													<div class="col-2 p-1"><?php do_action( 'simple-counter' ); ?></div>
													<div class="col-2 p-1"><?php do_action( 'simple-counter' ); ?></div>
													<div class="col-2 p-1"><?php do_action( 'simple-counter' ); ?></div>
												</div>
											</div>
										</div>
							    </div>
									<div class="col-sm-12 col-lg-12 col-xl-4 complex-menu__item">
										<div class="vertical-black-gradient background-section__wrapper">
										  <div class="background-section__image" style="background-image:url('http://localhost:8888/newcity/wp-content/images/aboutus.jpg')"></div>
										</div>
										<div class="complex-menu__item-content">
											<div class="complex-menu__title-wp center">
												<h2 class="complex-menu__title-firstline">Spatii <strong>Comerciale</strong></h2>
												<!-- <p class="complex-title-secondline">Comerciale</p> -->
												<button class="button light-button" value="Vezi detalii" style="left:initial;margin:auto;position:relative;"><span>Vezi detalii</spam></button>
											</div>
											<div class="complex-menu__report container">
												<div class="row">
													<div class="col-8 offset-2 p-1"><?php do_action( 'simple-counter' ); ?></div>
												</div>
											</div>
										</div>
							    </div>
							  </div>
							</div>
							<!-- COMPLEX SELECTOR END -->




							<!-- FLOOR MAP START -->
							<div class="container floor-map__container">
								<div class="row">
									<div class="col-12 col-md-3 floor-map__legend">

										<div class="floor-map__compass-img">
											<img src="<?php echo site_url() . "/wp-content/images/compass.png"; ?>">
										</div>
										<div class="floor-map__title">
											<h1>Etaj <span class="js-etaj-num">5</span></h1>
											<a class="floor-map__title--button link-button">schimba etajul</a>
										</div>
										<ul class="floor-map__legend-list bullet-list">
											<li class="three-rooms">apartamente cu <strong>trei camere</strong></li>
											<li class="two-rooms">apartamente cu <strong>doua camere</strong></li>
											<li class="one-room">apartamente cu <strong>o camera</strong></li>
										</ul>
									</div>
									<div class="col-12 col-md-9 floor-map__image">
									 <img src="<?php echo site_url() . "/wp-content/images/plan.jpg"; ?>">
									</div>
								</div>
							</div>
							<!-- FLOOR MAP END -->


							<?php hm_get_template_part( 'partials/corp_selector'
							                          , [ 'title' => "SELECTEAZA CORPUL CLADIRII PENTRU A VIZUALIZA APARTAMENTELE"
							                            , 'text' => "bloc corp"
							                            , 'count' => 6]); ?>


							<!-- APARTMENTS FILTER MENU START -->
							<div class="container filter-menu__container">
								<div class="row">
										<div class="filter-menu__wrapper">
											<span>Filtreaza dupa:</span>
											<nav class=" filter-menu__nav">
												<a>toate</a>
												<a>3 camere</a>
												<a>2 camere</a>
												<a>1 camera</a>
											</nav>
									</div>
								</div>
							</div>
							<!-- FLOOR MAP END -->





							<!-- BUTTONS START -->
							<div style="background:gray;padding:10px">
								<button class="button dark-button" value="Vezi apartamentele"><span>Vezi apartamentele<span></button>
								<button class="button light-button" value="Vezi detalii"><span>Vezi detalii<span></button>
								<button class="button light-button-contour" value="Vezi detalii"><span>Vezi detalii<span></button>
								<button class="button dark-button-contour" value="Vezi detalii"><span>Vezi detalii<span></button>
								<a class="link-button" href"#">schimba etajul</a>
							</div>
							<!-- BUTTONS END -->







					</main><!-- #main -->
				</div><!-- #primary -->
			</div><!-- .row end -->
	</div><!-- #content -->
</div><!-- #full-width-page-wrapper -->
