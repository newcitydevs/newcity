<?php
/**
 * Template Name: Apartments Page Template
 *
 * Template for displaying the complex apartments page.
 *
 * @package understrap-newcity
 */

if ( ! defined( 'ABSPATH' ) ) {exit; /* Exit if accessed directly.*/} ?>

<?php get_header(); ?>

<div class="row page-title">
  <div class="col-12">
    <h1 class="page-title__content">Apartamente</h1>
  </div>
</div>

<?php
  if(isset($_GET['complex'])){
    /* Extract the complex post. */
    $complex_args = [ 'p' => /*ID*/$_GET['complex'], 'post_type' => 'post' ];
    $complex_query = new WP_Query($complex_args);

    if($complex_query->have_posts()){
      /* Save the complex post. */
      $complex = $complex_query->posts[0];
      /* Reset wp_query loop. */
      wp_reset_postdata();

      /* Extract the buildings posts. */
      $buildings_args = [ 'category__in' => /*building*/3, 'post_parent' => $complex->ID, 'orderby' => 'title', 'posts_per_page' => -1, 'order' => 'ASC' ];
      $buildings_query = new WP_Query($buildings_args);

      if($buildings_query->have_posts()){
        /* Check if there is a selected building. */
        if(isset($_GET['bloc'])){
          $building = $buildings_query->posts[$_GET['bloc'] - 1];
        }

        /* Check which floors should be disabled. */
        $corpDisable = [];
        $details = [];
        foreach ($buildings_query->posts as $key => $_building) {
          /* Extract the details for the building. */
          $details[] = get_post_meta($_building->ID, 'detalii', TRUE);

          /* Extract the floors posts. */
          $floors_args = [ 'category__in' => /*floor*/4, 'post_parent' => $_building->ID, 'orderby' => 'meta_value_num', 'meta_key' => 'numar-etaj', 'posts_per_page' => -1, 'order' => 'DESC' ];
          $floors_query = new WP_Query($floors_args);
          $corpDisable[] = !$floors_query->have_posts();

          /* Reset wp_query loop. */
          wp_reset_postdata();
        }

        ?>
        <div class="container-fluid px-0" style="width:95%;">
          <div class="corp_selector__container-full-width partial">
            <?php
              hm_get_template_part( 'partials/corp_selector'
                                  , [ 'title' => "SELECTEAZA CORPUL CLADIRII &#8595;"
                                    , 'text' => "bloc corp"
                                    , 'url' => esc_url(get_page_link()) . "?complex=" . $_GET['complex'] . "&bloc="
                                    , 'count' => $buildings_query->found_posts
                                    , 'selected' => (isset($_GET['bloc'])) ? ($_GET['bloc']) : (-1)
                                    , 'disable' => $corpDisable
                                    , 'details' => $details]);
            ?>
          </div>
        </div>
        <?php
        /* Reset wp_query loop. */
        wp_reset_postdata();

        if(isset($building)){
          /* Extract the floors posts. */
          $floors_args = [ 'category__in' => /*floor*/4, 'post_parent' => $building->ID, 'orderby' => 'meta_value_num', 'meta_key' => 'numar-etaj', 'posts_per_page' => -1, 'order' => 'DESC' ];
          $floors_query = new WP_Query($floors_args);

          if($floors_query->have_posts()){
            /* Save the extracted posts. */
            $floors = $floors_query->posts;

            if(isset($_GET['etaj'])){
              $floor = $floors[abs($_GET['etaj'] - (sizeof($floors) - 1))];
            }
            /* Reset wp_query loop. */
            wp_reset_postdata();
          }
        }
      }
    }
  }
?>


<div class="container-fluid floor-menu__container js-floor-menu partial p-0">
  <?php if(isset($_GET['bloc'])):?>
  <div class="row floor-menu__tutorial-wp">
    <div class="col-12 floor-menu__tutorial-text">Selecteaza unul din etaje &#8595;</div>
  </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-10 col-md-8 col-lg-8 col-xl-6 floor-menu__info-side">
      <div class="floor-menu__complex-info">

          <h2 class="floor-menu__complex-info--name"><?= get_post_meta($complex->ID, 'nume-afisat', TRUE); ?></h2>
          <?php if(!isset($_GET['bloc'])):?>
          <p class="floor-menu__complex-info--description"><?= get_post_field('post_content', $complex->ID); ?></p>
        <?php endif; ?>


        <?php if(isset($_GET['bloc'])): ?>
          <h2 class="floor-menu__complex-info--location">
            <span>Corp <span class="js-corp-num"><?= $_GET['bloc']; ?></span>
            <?php if(isset($floor)): ?>&nbsp
            </span><span> Etaj <span class="js-etaj-num"><?= get_post_meta($floor->ID, 'nume-afisat', TRUE); ?></span></span>
            <?php endif; ?>
          </h2>
        <?php endif; ?>
        <?php if(isset($floor)): ?>
          <div class="floor-menu__complex-info--avability col-12">
            <?php
              /*******************************************************/
              /********************** 3 ROOMS ************************/
              /*******************************************************/
              /* Extract the 3 room apartments total count. */
              $three_rooms_total_apartments_args = [ 'category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_key' => 'numar-camere', 'meta_value' => '3', 'meta_compare' => '=', 'posts_per_page' => -1 ];
              $three_rooms_total_apartments_query = new WP_Query($three_rooms_total_apartments_args);
              if($three_rooms_total_apartments_query->have_posts()){
                /* Save the extracted posts number. */
                $three_rooms_total_count_apartments = $three_rooms_total_apartments_query->post_count;
              } else {
                /* Set the extracted posts number to 0. */
                $three_rooms_total_count_apartments = 0;
              }
              /* Reset wp_query loop. */
              wp_reset_postdata();

              /* Extract the 3 room apartments free count. */
              $three_rooms_free_apartments_args = [ 'category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_query' => [ 'relation' => 'AND', ['key' => 'numar-camere', 'value' => '3', 'compare' =>'='], ['key' => 'status', 'value' => 'disponibil', 'compare' =>'='] ], 'posts_per_page' => -1 ];
              $three_rooms_free_apartments_query = new WP_Query($three_rooms_free_apartments_args);
              if($three_rooms_free_apartments_query->have_posts()){
                /* Save the extracted posts number. */
                $three_rooms_free_count_apartments = $three_rooms_free_apartments_query->post_count;
              } else {
                /* Set the extracted posts number to 0. */
                $three_rooms_free_count_apartments = 0;
              }
              /* Reset wp_query loop. */
              wp_reset_postdata();

              /*******************************************************/
              /********************** 2 ROOMS ************************/
              /*******************************************************/
              /* Extract the 2 room apartments posts. */
              $two_rooms_total_apartments_args = [ 'category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_key' => 'numar-camere', 'meta_value' => '2', 'meta_compare' => '=', 'posts_per_page' => -1, ];
              $two_rooms_total_apartments_query = new WP_Query($two_rooms_total_apartments_args);
              if($two_rooms_total_apartments_query->have_posts()){
                /* Save the extracted posts number. */
                $two_rooms_total_count_apartments = $two_rooms_total_apartments_query->post_count;
              } else {
                /* Set the extracted posts number to 0. */
                $two_rooms_total_count_apartments = 0;
              }
              /* Reset wp_query loop. */
              wp_reset_postdata();

              /* Extract the 2 room apartments free count. */
              $two_rooms_free_apartments_args = [ 'category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_query' => [ 'relation' => 'AND', ['key' => 'numar-camere', 'value' => '2', 'compare' =>'='], ['key' => 'status', 'value' => 'disponibil', 'compare' =>'='] ], 'posts_per_page' => -1 ];
              $two_rooms_free_apartments_query = new WP_Query($two_rooms_free_apartments_args);
              if($two_rooms_free_apartments_query->have_posts()){
                /* Save the extracted posts number. */
                $two_rooms_free_count_apartments = $two_rooms_free_apartments_query->post_count;
              } else {
                /* Set the extracted posts number to 0. */
                $two_rooms_free_count_apartments = 0;
              }
              /* Reset wp_query loop. */
              wp_reset_postdata();

              /*******************************************************/
              /********************** 1 ROOM *************************/
              /*******************************************************/
              /* Extract the 1 room apartments posts. */
              $one_room_total_apartments_args = [ 'category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_key' => 'numar-camere', 'meta_value' => '1', 'meta_compare' => '=', 'posts_per_page' => -1 ];
              $one_room_total_apartments_query = new WP_Query($one_room_total_apartments_args);
              if($one_room_total_apartments_query->have_posts()){
                /* Save the extracted posts number. */
                $one_room_total_count_apartments = $one_room_total_apartments_query->post_count;
              } else {
                /* Set the extracted posts number to 0. */
                $one_room_total_count_apartments = 0;
              }
              /* Reset wp_query loop. */
              wp_reset_postdata();

              /* Extract the 1 room apartments free count. */
              $one_room_free_apartments_args = [ 'category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_query' => [ 'relation' => 'AND', ['key' => 'numar-camere', 'value' => '1', 'compare' =>'='], ['key' => 'status', 'value' => 'disponibil', 'compare' =>'='] ], 'posts_per_page' => -1 ];
              $one_room_free_apartments_query = new WP_Query($one_room_free_apartments_args);
              if($one_room_free_apartments_query->have_posts()){
                /* Save the extracted posts number. */
                $one_room_free_count_apartments = $one_room_free_apartments_query->post_count;
              } else {
                /* Set the extracted posts number to 0. */
                $one_room_free_count_apartments = 0;
              }
              /* Reset wp_query loop. */
              wp_reset_postdata();
            ?>

            <?php
              hm_get_template_part( 'partials/advanced_counter'
                                  , [ 'free-key' => "disponibile"
                                    , 'total-key' => "total"
                                    , 'free-value' => $three_rooms_free_count_apartments
                                    , 'total-value' => $three_rooms_total_count_apartments
                                    , 'description-intro' => "apartamente cu"
                                    , 'description-finish' => "3 camere"
                                    , 'css-class' => "three-rooms"]);

              hm_get_template_part( 'partials/advanced_counter'
                                  , [ 'free-key' => "disponibile"
                                    , 'total-key' => "total"
                                    , 'free-value' => $two_rooms_free_count_apartments
                                    , 'total-value' => $two_rooms_total_count_apartments
                                    , 'description-intro' => "apartamente cu"
                                    , 'description-finish' => "2 camere"
                                    , 'css-class' => "two-rooms"]);

              hm_get_template_part( 'partials/advanced_counter'
                                  , [ 'free-key' => "disponibile"
                                    , 'total-key' => "total"
                                    , 'free-value' => $one_room_free_count_apartments
                                    , 'total-value' => $one_room_total_count_apartments
                                    , 'description-intro' => "apartamente cu"
                                    , 'description-finish' => "1 camera"
                                    , 'css-class' => "one-room"]);
            ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
    <div class="col-2 col-md-4 col-lg-4 col-xl-6 floor-menu__building-side">
      <div class="background-section__wrapper">
        <div class="background-section__image floor-menu__building-side-wrapper" style="background-image:url(<?= site_url() . get_post_meta($complex->ID, 'imagine-selector-eraj', TRUE); ?>)">
          <?php
            if(isset($floors)){
              ?> <nav class="floor-menu__nav"> <?php
              foreach($floors as $index=>$_floor) {
                ?> <a href="<?= esc_url(get_page_link()) . "?complex=" . $_GET['complex'] . "&bloc=" . $_GET['bloc'] . "&etaj=" . abs((sizeof($floors) - 1) - $index); ?>" class="<?= ( (isset($floor) && $floor->ID == $_floor->ID) ? ('floor-menu__nav--selected') : ('') ); ?>"><?= get_post_meta($_floor->ID, 'nume-afisat', TRUE); ?></a> <?php
              }
              ?> </nav> <?php
            }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>


<?php if(isset($floor)){
    /* Extract the apartments posts. */
    if(isset($_GET['filtru'])){
      switch ($_GET['filtru']) {
        case '3-camere':
          $apartments_args = ['category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_query' => [['key' => 'numar-camere', 'value' => '3', 'compare' =>'=']], 'orderby' => 'meta_value_num', 'meta_key' => 'nume-afisat', 'posts_per_page' => -1, 'order' => 'ASC'];
          break;
        case '2-camere':
          $apartments_args = ['category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_query' => [['key' => 'numar-camere', 'value' => '2', 'compare' =>'=']], 'orderby' => 'meta_value_num', 'meta_key' => 'nume-afisat', 'posts_per_page' => -1, 'order' => 'ASC'];
          break;
        case '1-camera':
          $apartments_args = ['category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'meta_query' => [['key' => 'numar-camere', 'value' => '1', 'compare' =>'=']], 'orderby' => 'meta_value_num', 'meta_key' => 'nume-afisat', 'posts_per_page' => -1, 'order' => 'ASC'];
          break;
        default:
          $apartments_args = ['category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'orderby' => 'meta_value_num', 'meta_key' => 'nume-afisat', 'posts_per_page' => -1, 'order' => 'ASC'];
          break;
      }
    } else {
      $apartments_args = [ 'category__in' => /*apartment*/5, 'post_parent' => $floor->ID, 'orderby' => 'meta_value_num', 'meta_key' => 'nume-afisat', 'posts_per_page' => -1, 'order' => 'ASC' ];
    }
    $apartments_query = new WP_Query($apartments_args);

    if($apartments_query->have_posts()){
      /* Save the extracted posts. */
      $apartments = $apartments_query->posts;
  ?>


    <!-- APARTMENTS FILTER MENU START -->
    <div class="container filter-menu__container js-apartments-filter">
      <div class="row">
          <div class="filter-menu__wrapper">
            <span>Filtreaza dupa:</span>
            <nav class="filter-menu__nav">
              <?php
                $filters=['toate' => "toate", '3-camere' => "3 camere", '2-camere' => "2 camere", '1-camera' => "1 camera"];

                foreach ($filters as $key => $filter) {
                  ?><a href="<?= esc_url(get_page_link()) . "?complex=" . $_GET['complex'] . "&bloc=" . $_GET['bloc'] . "&etaj=" . $_GET['etaj'] . "&filtru=" . $key; ?>" class="<?= (!isset($_GET['filtru']) && $key==="toate") || (isset($_GET['filtru']) && ($_GET['filtru'] === $key)) ? ("filter-menu__nav--selected") : (""); ?>"><?= $filter; ?></a><?php
                }
              ?>
            </nav>
        </div>
      </div>
    </div>
    <!-- APARTMENTS FILTER MENU END -->

  <!-- FLOOR MAP START -->
    <div class="container custom-md-fluid floor-map__container partial">
      <div class="row">
        <div class="col-12 col-md-3 floor-map__legend">
          <div class="floor-map__title">
            <h3>Plan Etaj <span class="js-etaj-num"><?= get_post_meta($floor->ID, 'nume-afisat', TRUE); ?></span></h3>
            <a class="floor-map__title--button link-button js-floor-menu-btn">schimba etajul</a>
          </div>

          <ul class="floor-map__legend-list bullet-list">
            <li class="three-rooms">apartamente cu <strong>trei camere</strong></li>
            <li class="two-rooms">apartamente cu <strong>doua camere</strong></li>
            <li class="one-room">apartamente cu <strong>o camera</strong></li>
          </ul>

        </div>
        <div class="col-11 col-md-9 offset-1 offset-md-0 floor-map__image-wp">
          <div class="row">
            <div class="col-1 col-md-1 m-0 p-0 p-md-1">
              <div class="map__compass-img">
                <img src="<?= site_url() . get_post_meta($complex->ID, 'imagine-busola', TRUE); ?>">
              </div>
            </div>
            <div class="col-9 col-md-10">
              <div class="floor-map__image">
                <img src="<?= site_url() . get_post_meta($complex->ID, 'cale-media-complex', TRUE) . get_post_meta($building->ID, 'cale-media-bloc', TRUE) . '/' . get_post_meta($floor->ID, 'cale-media-etaj', TRUE) . ".jpg"; ?>">
                <div class="floor-map__image-masks">
                  <?php foreach ($apartments as $key => $apartment) {
                    $image_url = site_url() . get_post_meta($complex->ID, 'cale-media-complex', TRUE) . get_post_meta($building->ID, 'cale-media-bloc', TRUE) . '/maps/' . get_post_meta($apartment->ID, 'cale-media-apartament', TRUE) . ".png"; ?>
                    <div class="img camere-<?= get_post_meta($apartment->ID, 'numar-camere', TRUE); ?> <?= ('vandut' == get_post_meta($apartment->ID, 'status', TRUE)) ? ('sold') : ('') ?>" style="-webkit-mask-image:url('<?=$image_url?>');-o-mask-image:url('<?=$image_url?>');-moz-mask-image:url('<?=$image_url?>');mask-image:url('<?=$image_url?>')">"></div>
                  <?php }?>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- FLOOR MAP END -->

  <div class="container custom-md-fluid apartment__wrapper py-0 partial">
	  	<?php foreach ($apartments as $key => $apartment) {
		  	$apartment->name = get_post_meta($apartment->ID, 'nume-afisat', TRUE);
		  	$apartment->number = explode(" ", $apartment->name)[1];
/*
		  	print_r("<pre>");
		  	print_r($apartment);
		  	print_r("</pre>");
*/
		}
		function cmp($a, $b)
		{
		    return intval($a->number) > intval($b->number);
		}
// 		print_r($apartments);
		usort($apartments, "cmp");
		foreach ($apartments as $key => $apartment) { ?>
          <div class="row my-2 apartment__row bg-white <?= ('vandut' == get_post_meta($apartment->ID, 'status', TRUE)) ? ('sold') : ('') ?>">
            <?php
              $js_ap_location = '';
              $js_ap_location = '';
              if($_GET['complex'] == 38){
                $js_ap_location = $js_ap_location.'Complex:Eroilor';
              }elseif($_GET['complex'] == 44){
                $js_ap_location = $js_ap_location.'Complex:Cetatii';
              }
              $js_ap_location = $js_ap_location." Bloc:".$_GET['bloc']." Etaj:".get_post_meta($floor->ID, 'nume-afisat', TRUE)." ".get_post_meta($apartment->ID, 'nume-afisat', TRUE);
            ?>
            <input type="hidden" name="full_ap_location" value="<?= $js_ap_location ?>">
            <div class="col-12 col-md-4 apartment__picture-wp">
              <div class="container">
                <div class="row">
                  <div class="col-2 col-md-2 m-0 p-0 p-md-2">
                    <div class="map__compass-img">
                      <img src="<?= site_url() . get_post_meta($complex->ID, 'imagine-busola', TRUE); ?>">
                    </div>
                  </div>
                  <div class="col-8 col-md-8 apartment__picture">
                    <img src="<?= site_url() . get_post_meta($complex->ID, 'cale-media-complex', TRUE) . get_post_meta($building->ID, 'cale-media-bloc', TRUE) . '/' . get_post_meta($floor->ID, 'cale-media-etaj', TRUE) . ".jpg"; ?>">
                    <?php $image_url = site_url() . get_post_meta($complex->ID, 'cale-media-complex', TRUE) . get_post_meta($building->ID, 'cale-media-bloc', TRUE) . '/maps/' . get_post_meta($apartment->ID, 'cale-media-apartament', TRUE) . ".png"; ?>
                    <div class="img camere-<?= get_post_meta($apartment->ID, 'numar-camere', TRUE); ?>" style="-webkit-mask-image:url('<?= $image_url; ?>');-o-mask-image:url('<?= $image_url; ?>');-moz-mask-image:url('<?= $image_url; ?>');mask-image:url('<?= $image_url; ?>')">"></div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-12 col-md-8 apartment__data sold">
              <div class="row">
                <div class="col-12">
                  <h1 class="apartment__head-title"><?= get_post_meta($apartment->ID, 'nume-afisat', TRUE); ?></h1>
                  <p class="apartment__head-info">corp: <strong><?= $_GET['bloc']; ?></strong>&nbsp etaj: <strong><?= get_post_meta($floor->ID, 'nume-afisat', TRUE); ?></strong></p>
                  <p class="apartment__head-info">numar camere: <strong><?= get_post_meta($apartment->ID, 'numar-camere', TRUE); ?></strong></p>
                  <hr/>
                </div>
              </div>  <div class="row apartment__surface">
                <div class="col-12 col-md-4 col-lg-3">
                  <p><strong>Suprafata utila:</strong></p>
                  <p class="apartment__surface-all"><?= get_post_meta($apartment->ID, 'suprafata-utila', TRUE); ?> m<sup>2</sup></p>
                </div>

                <div class="col-12 col-md-4 col-lg-5 apartment__surface-details">
                  <?= get_post_field('post_content', $apartment->ID); ?>
                </div>

                <div class="col-12 col-md-4">
                  <div class="row">
                    <div class="col-12 col-md-12 mb-2 apartment__offer-button-wrapper pdf-buttons-wrapper">
                      <button type="button" class="button dark-button-contour apartment__button js-desktop-btn d-none" data-toggle="modal" data-target="#popup_pdf" data-plan="<?= site_url() . get_post_meta($complex->ID, 'cale-media-complex', TRUE) . get_post_meta($building->ID, 'cale-media-bloc', TRUE) . '/plans/' . get_post_meta($apartment->ID, 'cale-media-apartament', TRUE) . ".pdf"; ?>" value="Vezi detalii"><span>Vezi detalii<span></button>
                      <a class="button dark-button-contour apartment__button js-mobile-btn d-block" target="_blank" href="<?= site_url() . get_post_meta($complex->ID, 'cale-media-complex', TRUE) . get_post_meta($building->ID, 'cale-media-bloc', TRUE) . '/plans/' . get_post_meta($apartment->ID, 'cale-media-apartament', TRUE) . ".pdf"; ?>"><span>Vezi detalii</span></a>
                    </div>
                    <div class="col-12 col-md-12">
                      <button class="button dark-button apartment__button js-get_offer-btn" data-toggle="modal" data-target="#popup_form" value="Cere oferta"><span>Cere oferta<span></button>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        <?php
        }
        /* Reset wp_query loop. */
        wp_reset_postdata();
      } else { ?>
        <div class="apartments__no-result"><span>Nu a fost gasit niciun rezultat.</span></div>
    <?php } ?>
  </div>
<?php } ?>


<?php hm_get_template_part( 'partials/popup_detalii_apartament', []); ?>
<?php hm_get_template_part( 'partials/popup_form', []); ?>
<?php hm_get_template_part( 'partials/dynamic-footer'); ?>
