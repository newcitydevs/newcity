<?php
/**
 * Template Name: Cetatii Description Page Template
 *
 * Template for displaying the Cetatii complex description page.
 *
 * @package understrap-newcity
 */

if ( ! defined( 'ABSPATH' ) ) {exit; /* Exit if accessed directly.*/} ?>

<?php
  $one_room_total_count_apartments = 0;
  $two_room_total_count_apartments = 0;
  $three_room_total_count_apartments = 0;
  $total_surface = 0;
  $total_parking_spots = 0;

  /* Extract the complex post. */
  $complex_query = new WP_Query([ 'p' => /*ID*/44, 'post_type' => 'post' ]);
  if($complex_query->have_posts()){
    /* Save the complex post. */
    $complex = $complex_query->posts[0];
    $complexName = explode(" ", get_post_meta($complex->ID, 'nume-afisat', TRUE));
    /* Reset wp_query loop. */
    wp_reset_postdata();

    get_complex_characteristics( /*CETATII_ID*/44
                                , $one_room_total_count_apartments
                                , $two_room_total_count_apartments
                                , $three_room_total_count_apartments
                                , $total_surface
                                , $total_parking_spots);
  }
?>

<?php get_header(); ?>


<div class="container-fluid complex-picture__container partial bg-white">
  <div class="row">
    <div class="col-12 complex-picture__item">
      <div class="vertical-black-gradient background-section__wrapper">
        <div class="background-section__image" style="background-image:url(<?= site_url() . "/wp-content/images/cetatii/cetatii-panoram.jpg"; ?>)"></div>
      </div>

      <div class="complex-picture__item-content">
        <div class="complex-title__wp <?= (isset($item['title']['style'])) ? ($item['title']['style']) : (''); ?>">
          <h2 class="complex-title__firstline"><?= $complexName[0]; ?><strong><?= $complexName[1]; ?></strong></h2>
          <h2 class="complex-title__secondline"><?= $complexName[2]; ?></h2>
          <a href="<?= site_url() . '/apartamente/?complex=44'?>" class="button red-button" value="Vezi apartamentele"><span>Vezi apartamentele</span></a>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container py-0 complex-info__wrapper partial bg-white">
  <div class="row">
    <div class="col-12 col-md-6">
      <div class="container complex-info__data">
        <div class="row">
        <h2><?= get_post_meta($complex->ID, 'nume-afisat', TRUE); ?></h2>
          <p>Ansamblul residential New City Cetatii iti pune la dizpozitie apartamente cu diferite variante de compartimentare ce dispun de balcoane si terase de tip logie cu  dimensiuni variabile. In plus, apartamentele de la parter dispun de gradina cu suprafete cuprinse intre 35mp - 42mp. Fiind situat intr-o zona ferita de agitatia urbana, complexul nostru este inconjurat de spatii verzi si locuri de joaca pentru copii.</p>
          <p>Materialele folosite sunt de cea mai buna calitate: caramida de Porotherm, profil PVC cu 3 foi de sticla, izolatie de exterior realizata cu polistiren expandat ignifugat ESP-80 de 10 cm, totodata fiind izolat fonic si terminc intre placi. Balcoanele si terasele sunt finisate de catre noi, cu balustrazi si placate cu placi ceramice antiderapante (pentru inghet).</p>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-6 px-0">
      <div class="container complex-info__data complex-info__data--counters">
        <div class="row">
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => $one_room_total_count_apartments, 'description' => "apartamente cu 1 camera"]); ?></div>
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => $two_room_total_count_apartments, 'description' => "apartamente cu 2 camera"]); ?></div>
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => $three_room_total_count_apartments, 'description' => "apartamente cu 3 camera"]); ?></div>
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => $total_surface, 'description' => "m<sup>2</sup> de spatii de locuit"]); ?></div>
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => $total_parking_spots, 'description' => "locuri de parcare"]); ?></div>
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => get_post_meta($complex->ID, 'locuri-joaca', TRUE), 'description' => "m<sup>2</sup> de locuri de joaca"]); ?></div>
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => get_post_meta($complex->ID, 'spatii-verzi', TRUE), 'description' => "m<sup>2</sup> de spatii verzi"]); ?></div>
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => get_post_meta($complex->ID, 'suprafata-construita', TRUE), 'description' => "m<sup>2</sup> de suprafata construita"]); ?></div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php hm_get_template_part( 'partials/facilitati') ?>

<div class="container partial bg-white">
  <div class="row">
    <div class="col-12">
        <h2 class="horizontal-description-text-title">Galerie foto Cetatii</h2>
    </div>
  </div>
  <div class="mx-auto">
    <?= do_shortcode('[modula id="457"]'); ?>
  </div>
</div>


<?php hm_get_template_part( 'partials/location_map'
                          , [ 'title' => "LOCALIZARE"
                            , 'address' => "Str. Cetatii, nr. 322, Comuna Floresti, Jud. Cluj"
                            , 'map' => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2734.448630373851!2d23.48421351546454!3d46.736335679136495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47491028e617b58f%3A0x782155f670382c83!2zU3RyYWRhIENldMSDyJtpaSAzMjIsIEZsb3JlyJl0aSA0MDcyODA!5e0!3m2!1sro!2sro!4v1552471475403"
                            , 'items' => [
                              // ['title' => [ 'up' => "1.3 km", 'down' => "scoala"], 'description' => "La mai putin de 1.5 km fata de ansamblul rezidential aveti la dispozitie unitati de invatamant: scoala si liceu."]
                              //            , ['title' => [ 'up' => "200 m", 'down' => "supermarket"], 'description' => "la doar 4 minute de mers aveti la dispozitie supermarketuri, cabinete veterinare, unitati bancare."]
                              //            , ['title' => [ 'up' => "1 km", 'down' => "teren tenis"], 'description' => "Va puteti petrece timpul liber la salile de fitness din apropriere sau jucand tenis de camp, la circa 1 km."]
                              //            , ['title' => [ 'up' => "1 km", 'down' => "spital"], 'description' => "Veti putea beneficia in viitorul apropiat si de serviciile Spitalului Regional de Urgenta Floresti, situat la 1 km."]
                              //            , ['title' => [ 'up' => "300 m", 'down' => "gradinita"], 'description' => "Ansamblul rezidential va ofera de asemenea acces facil, la doar 300 m distanta, si la o gradinita."]
                              //            , ['title' => [ 'up' => "1 km", 'down' => "piata"], 'description' => "Piata Floresti se afla la doar 200m. Totodata, complexul comercial VIVO Mall se afla la doar 3 km."]
                                         ]
                            ]); ?>

<?php hm_get_template_part( 'partials/dynamic-footer',[]); ?>
