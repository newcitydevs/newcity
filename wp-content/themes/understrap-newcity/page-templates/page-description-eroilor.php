<?php
/**
 * Template Name: Eroilor Description Page Template
 *
 * Template for displaying the Eroilor complex description page.
 *
 * @package understrap-newcity
 */

if ( ! defined( 'ABSPATH' ) ) {exit; /* Exit if accessed directly.*/} ?>

<?php
  $one_room_total_count_apartments = 0;
  $two_room_total_count_apartments = 0;
  $three_room_total_count_apartments = 0;
  $total_surface = 0;
  $total_parking_spots = 0;

  /* Extract the complex post. */
  $complex_query = new WP_Query([ 'p' => /*ID*/38, 'post_type' => 'post' ]);
  if($complex_query->have_posts()){
    /* Save the complex post. */
    $complex = $complex_query->posts[0];
    $complexName = explode(" ", get_post_meta($complex->ID, 'nume-afisat', TRUE));
    /* Reset wp_query loop. */
    wp_reset_postdata();

    get_complex_characteristics( /*EROILOR_ID*/38
                               , $one_room_total_count_apartments
                               , $two_room_total_count_apartments
                               , $three_room_total_count_apartments
                               , $total_surface
                               , $total_parking_spots);
  }
?>

<?php get_header(); ?>


<div class="container-fluid complex-picture__container partial bg-white">
  <div class="row">
    <div class="col-12 complex-picture__item">
      <div class="vertical-black-gradient background-section__wrapper">
        <div class="background-section__image" style="background-image:url(<?= site_url() . '/wp-content/images/eroilor/eroilor-panoram.jpg'; ?>)"></div>
      </div>
      <div class="complex-picture__item-content">
        <div class="complex-title__wp <?= (isset($item['title']['style'])) ? ($item['title']['style']) : (''); ?>">
          <h2 class="complex-title__firstline"><?= $complexName[0]; ?><strong><?= $complexName[1]; ?></strong></h2>
          <h2 class="complex-title__secondline"><?= $complexName[2]; ?></h2>
          <a href="<?= site_url() . '/apartamente/?complex=38'?>" class="button red-button" value="Vezi apartamentele"><span>Vezi apartamentele</span></a>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container py-0 complex-info__wrapper partial bg-white">
  <div class="row">
    <div class="col-12 col-md-6">
      <div class="container complex-info__data">
        <div class="row">
        <h2><?= get_post_meta($complex->ID, 'nume-afisat', TRUE); ?></h2>
          <p>Ansamblul beneficiaza de un concept arhitectural modern, acesta oferind 5 blocuri cu apartamente cu 1,2 si 3 camere  cu suprafete cuprinse intre 37-76 mp cu balcoane si terase de tip logie de diferite dimensiuni. Apartamentele se predau in stadiul semifinisat, acest lucru insemnand tencuieli si strat de glet, șapă, tâmplărie cu profil format din minim 5 camere si 3 foi de sticla, centrală termica, calorifere, usă metalica la intrarea în apartament cu un design modern, instalaţiile electrice, termice şi sanitare trase la poziţie sau in stadiul finisat, acest lucru insemnand gresie, faianta, parchet, lavabil, usi interioare, prize si intrerupatoare, centrala termica si calorifere.</p>
          <!-- <p>Lucram doar cu materiale de cea mai buna calitate: caramida de Porotherm, profil PVC cu 3 foi de sticla, izolatie de exterior realizata cu polistiren expandat ignifugat ESP-80 de 10 cm, totodata este izolat si fonic si terminc intre placi. Balcoanele si terasele sunt finisate de catre noi, cu balustrazi si placate cu placi ceramice antiderapante (pentru inghet).</p> -->
          <p>Aerisit, spatios dar si luminos, ansamblul nostru este inconjurat de spatii verzi, cu parcuri generoase si locuri de joaca pentru copii la fiecare bloc. De asemenea punem la dispozitie alei pe care va puteti plimba sau chiar alerga. Alegerea locuintei este o decizie foarte importanta pentru tine si familia ta. De aceea, noi iti punem la dispozitie modalitati de plata ce pot fi personalizate in functie de nevoile tale, iar vanzarile se fac direct de la dezvoltator, ceea ce inseamna zero comision.</p>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-6 px-0">
      <div class="container complex-info__data complex-info__data--counters">
        <div class="row">
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => $one_room_total_count_apartments, 'description' => "apartamente cu 1 camera"]); ?></div>
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => $two_room_total_count_apartments, 'description' => "apartamente cu 2 camera"]); ?></div>
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => $three_room_total_count_apartments, 'description' => "apartamente cu 3 camera"]); ?></div>
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => $total_surface, 'description' => "m<sup>2</sup> de spatii de locuit"]); ?></div>
          <div class="col col-lg-4"><?= hm_get_template_part( 'partials/simple_counter', ['value' => $total_parking_spots, 'description' => "locuri de parcare"]); ?></div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php hm_get_template_part( 'partials/facilitati') ?>

<div class="container partial bg-white">
  <div class="row">
    <div class="col-12">
        <h2 class="horizontal-description-text-title">Galerie foto Eroilor</h2>
    </div>
  </div>
  <div class="mx-auto">
    <?= do_shortcode('[modula id="448"]'); ?>
  </div>
</div>

<?php hm_get_template_part( 'partials/location_map'
                          , [ 'title' => "LOCALIZARE"
                            , 'address' => "Str. Eroilor, nr. 378, Comuna Floresti, Jud. Cluj"
                            , 'map' => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2734.6054572061735!2d23.482809489857082!3d46.733242922537485!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4749102f152e44f5%3A0xf3aa7b1c8eda5026!2sNew+City+Eroilor!5e0!3m2!1sen!2sro!4v1549275780973"
                            , 'items' => [ ['title' => [ 'up' => "2 km", 'down' => "scoala"], 'description' => "La mai putin de 2 km fata de ansamblul rezidential aveti la dispozitie unitati de invatamant: gradinita, scoala si liceu"]
                                         , ['title' => [ 'up' => "500 m", 'down' => "oficiu postal"], 'description' => "Oficiul Postal la 500m distanta"]
                                         , ['title' => [ 'up' => "700 m", 'down' => "politie"], 'description' => "Sediul Politiei Floresti 700m distanta"]
                                         , ['title' => [ 'up' => "20 m", 'down' => "spital"], 'description' => "Statia ISU - Smurd Floresti se afla la 20m departare"]
                                         , ['title' => [ 'up' => "200 m", 'down' => "supermarket"], 'description' => "Numeroase supermarket-uri la doar 200m distanta"]
                                         , ['title' => [ 'up' => "vecinatate", 'down' => "fitness"], 'description' => "Sala de fitness in imediata vecinatate a ansamblului"]
                                         , ['title' => [ 'up' => "30 m", 'down' => "veterinar"], 'description' => "Cabinet Veterinar la numai 30m"]
                                         , ['title' => [ 'up' => "1.5 km", 'down' => "primarie"], 'description' => "Primaria se afla la mai putin la 1.5 km"]
                                         , ['title' => [ 'up' => "1.5 km", 'down' => "banca"], 'description' => "Sucursala Banca Transilvania la 1.5 km, dar si un ATM in apropierea Pietei Floresti"]]
                            ]); ?>

<?php hm_get_template_part( 'partials/dynamic-footer',[]); ?>
