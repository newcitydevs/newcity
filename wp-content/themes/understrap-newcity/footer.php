<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */
if ( ! defined( 'ABSPATH' ) ) {exit; /* Exit if accessed directly. */} ?>
        <div class="partial p-0"><!-- footer-wrapper-start -->
          <div class="container">
            <div class="blockquote text-center m-0 pt-4">
              <hr class="m-1 thick-separator"/>
              <span>
                <i class="fa fa-copyright footer-copyright-icon"></i>
                <span class="footer-copyright-text">NewCityApartments - Toate drepturile rezervate.</span>
              </span>
            </blockquote>
          </div>
        </div>
      </div><!-- footer-wrapper-end -->
    </div><!-- page-wrapper-end -->
    <?php wp_footer(); ?>
  </body>
</html>
