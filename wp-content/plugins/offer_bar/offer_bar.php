<?php

/**
* Plugin Name: Offer Bar
* Plugin URI: http://www.mainwp.com
* Description:
* Version: 1.0.0
* Author: Michael
* Author URI:
* License: GPL2
*/
add_action('admin_menu', 'my_admin_menu');

function my_admin_menu () {
  add_management_page('Offer Bar', 'Offer Bar', 'manage_options', __FILE__, 'offer_bar_admin_page');
}

function offer_bar_admin_page () {
  $textvar = get_option('offer_bar_text', '');
  if (isset($_POST['change-clicked'])) {
    update_option( 'offer_bar_text', $_POST['footertext'] );
    $textvar = get_option('offer_bar_text', '');
  }
?>
<div class="wrap">
  <h1>Offer Bar</h1>
  <form action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="post">
    Continut Oferta:
    <input style="width:100%;text-align:center;" type="text" value="<?php echo $textvar; ?>" name="footertext" placeholder=""><br />
    <input name="change-clicked" type="hidden" value="1" />
    <input type="submit" value="Schimba textul ofertei" />
  </form>
</div>
<?php
}
?>
